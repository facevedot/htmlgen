import os
import re
import sys
from collections import deque
import htmlmin
import bs4
import requests
from html.parser import HTMLParser
from csscompressor import compress


class MyHTMLParser(HTMLParser):
    temp_file = None
    temp_filename = "tmp.txt"
    tag = None

    def set_tag(self, tag):
        self.tag = tag

    def get_parsed(self):
        file = open(self.temp_filename, "rb")
        content = file.read()
        file.close()
        return content

    def handle_starttag(self, tag, attrs):
        if self.temp_file is None:
            if tag == self.tag:
                self.temp_file = open(self.temp_filename, "wb")

    def handle_endtag(self, tag):
        if self.temp_file is not None:
            self.temp_file.close()
            self.temp_file = None

    def handle_data(self, data):
        if self.temp_file is not None:
            self.temp_file.write(data)


def extract_content(data, tag):
    found = False
    content = ""
    for line in data.splitlines():
        if ("<" + tag) in line:
            found = True
        elif ("</" + tag) in line:
            found = False
        else:
            if found:
                content += line + "\n"
    return content


def replace_content(data, tag, new_content):
    found = False
    replaced = False
    new_data = ""
    for line in data.splitlines():
        if ("<" + tag) in line:
            if not replaced:
                new_data += line + "\n"
                new_data += new_content + "\n"
                new_data += "</" + tag + ">\r\n"
                replaced = True
            found = True
        elif found:
            if ("</" + tag) in line:
                found = False
            continue
        else:
            new_data += line + "\n"

    return new_data


def minify_javascript(js_raw):
    # WHITESPACE_ONLY SIMPLE_OPTIMIZATIONS ADVANCED_OPTIMIZATIONS
    parametros = {'compilation_level': "SIMPLE_OPTIMIZATIONS",
                  'output_format': "text",
                  'output_info': "compiled_code",
                  'js_code': js_raw
                  }
    r = requests.post('https://closure-compiler.appspot.com/compile', data=parametros)
    return r.text


# Minifiying Fucntion for JS
def minify_js(scripts, path):
    if debug_on:
        print("\nRead Scripts:")
    # create list of script srcs
    scriptsSrc = deque()
    for script in scripts:
        scriptsSrc.append(path + script.attrs["src"])
        if debug_on:
            print("\t" + path + script.attrs["src"])
    # merge scripts to temp.js
    if debug_on:
        print("\nMerge Scripts:")
        print("\t", end="")
    with open("temp.js", "w") as outfileScript:
        for fname in scriptsSrc:
            # add space every script
            outfileScript.write("\n")
            if debug_on:
                print("~", end="")
            with open(fname) as infile:
                for line in infile:
                    if debug_on:
                        print(line, end="")
                    outfileScript.write(line)


# Minifiying Fucntion for CSS
def minify_css(stylesheets, path):
    # create list of stylesheets srcs
    if debug_on:
        print("\nRead Stylesheets:")
    stylesheetsSrc = deque()
    for stylesheet in stylesheets:
        stylesheetsSrc.append(path + stylesheet.attrs["href"])
        if debug_on:
            print("\t" + path + stylesheet.attrs["href"])
    # merge stylsheets to temp.css
    if debug_on:
        print("\n")
        print("Merge Stylesheets:")
        print("\t", end="")
    with open("temp.css", "w") as outfileCSS:
        for fname in stylesheetsSrc:
            # add space every script
            outfileCSS.write("\n")
            if debug_on:
                print("~", end="")
            with open(fname) as infile:
                for line in infile:
                    if debug_on:
                        print(line, end="")
                    outfileCSS.write(line)


# MAIN BODY
def main():
    with os.scandir('.') as entries:
        for entry in entries:
            if os.path.isfile(os.path.join('.', entry)):
                ext = os.path.splitext(entry.name)[-1].lower()
                if ext == ".html":
                    src_file = entry.name
                    target_file = entry.name
                    print("Currently generating and minifying " + src_file + "\n")
                    path = re.sub(r"[^/]*$", "", src_file)
                    soup = bs4.BeautifulSoup(open(src_file, encoding="utf-8"), features="html5lib",
                                             from_encoding="utf-8")
                    try:  # This is just in case we dont find a JS src script
                        lastScript = soup.findAll("script", attrs={"src": True})[-1]
                        scripts = soup.findAll("script", attrs={"src": True})
                    except IndexError:
                        print("No script with src attribute found in " + src_file + "\n")
                        continue
                    try:  # This is just in case we dont find a css href
                        lastStylesheet = soup.findAll("link", attrs={"rel": "stylesheet"})[-1]
                        stylesheets = soup.findAll("link", attrs={"rel": "stylesheet"})
                    except IndexError:
                        print("No href to css file found in " + src_file + "\n")
                        continue

                    minify_js(scripts, path)
                    minify_css(stylesheets, path)

                    # minify javascript
                    if debug_on:
                        print("\n")
                        print("Minify temp.js\n\t~")
                    with open("temp.js") as js:
                        minified_js = js.read()

                    # minify css
                    if debug_on:
                        print("\nMinify temp.css\n\t~")
                    with open("temp.css") as css:
                        minified_css = compress(css.read())

                    # replace scripts with merged and min embed script / css
                    if debug_on:
                        print("\nReplacing and deleting\n\t~")
                    tag = soup.new_tag("script")
                    tag["type"] = "text/javascript"
                    tag.append(minified_js)
                    lastScript.replace_with(tag)

                    tag = soup.new_tag("style")
                    tag["type"] = "text/css"
                    tag.append(minified_css)
                    lastStylesheet.replace_with(tag)

                    # remove script and style tags
                    for script in scripts:
                        script.decompose()
                    for stylesheet in stylesheets:
                        stylesheet.decompose()

                    # remove temp
                    os.remove("temp.js")
                    os.remove("temp.css")

                    # move old html to new folder
                    file_name = src_file.split(os.path.sep)[-1]
                    os.makedirs("./OLD", exist_ok=True)
                    try:
                        os.rename(src_file, os.path.join("./OLD", file_name))
                    except FileExistsError:
                        print("Replacing " + src_file + " in OLD dir \n")
                        os.remove("./OLD/" + src_file)
                        os.rename(src_file, os.path.join("./OLD", file_name))
                    # save html as target
                    file = open(target_file, "w", encoding="utf-8")
                    soup.prettify(formatter="minimal")
                    dammit = bs4.UnicodeDammit.detwingle(soup.encode("utf-8"))

                    content = dammit.decode("utf-8")

                    javascript_raw = extract_content(content, "script")
                    if debug_on:
                        print("\n Java minified even further \n\t~")
                        print(javascript_raw)
                    java_minified = minify_javascript(javascript_raw)

                    content = replace_content(content, "script", java_minified)

                    if debug_on:
                        print("\n Minifying whole HTML\n\t~")

                    brand_new_html = htmlmin.minify(content, True, True, True)

                    if debug_on:
                        print("\n------------------------------\n")
                        print(brand_new_html)
                        print("\n------------------------------\n")

                    print("Finishing up " + src_file + "\n")

                    file.write(brand_new_html)

                    file.close()

                    print("FINISHED WITH " + target_file + " ---------- \n \n")
    print("-FIN-\n")


if __name__ == "__main__":
    debug_on = True
    if len(sys.argv) == 2:
        if sys.argv[1] == "1":
            debug_on = True
            print("DEBUG")
        else:
            print("\n INVALID ARGUMENT \n only valid argument is 1 for debug text \n")
            exit(1)
    elif len(sys.argv) > 2:
        print("\n TOO MANY ARGUMENTS \n only valid argument is 1 for debug text \n")
        exit(1)
    else:
        debug_on = False
    main()
# MAIN BODY END
