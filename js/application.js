
if (!window.console) console = {log: function() {}};

function d(id) { return document.getElementById(id); }

function q(s) { return document.querySelector(s); }

function qa(s) { return document.querySelectorAll(s); }

//Implements the element.textContent accessor in IE8.
try {
	// from Eli Grey @ http://eligrey.com/blog/post/textcontent-in-ie8
	if (Object.defineProperty && Object.getOwnPropertyDescriptor &&
		Object.getOwnPropertyDescriptor(Element.prototype, "textContent") &&
		!Object.getOwnPropertyDescriptor(Element.prototype, "textContent").get) {
		var innerText = Object.getOwnPropertyDescriptor(Element.prototype, "innerText");
		Object.defineProperty(Element.prototype, "textContent", {
			// It won't work if you just drop in innerText.get
			// and innerText.set or the whole descriptor.
			get: function() {
				return innerText.get.call(this)
			},
			set: function(x) {
				return innerText.set.call(this, x)
			}
		});
	}
} catch (e) {
	// bad Firefox
}

//http://stackoverflow.com/questions/12744202/undefined-is-not-a-function-evaluating-el-click-in-safari
//http://insta-tech.blogspot.com.es/2013/08/browser-error-typeerror-undefined-is.html
//var myeve = document.createEvent("HTMLEvents");
//myeve.initEvent("click", true, true);

// cross browser way to add an event listener
function addListener(obj, event, fn) {
	/*
    if (obj.addEventListener) {
        obj.addEventListener(event, fn, false);   // modern browsers
    } else {
        obj.attachEvent("on"+event, fn);          // older versions of IE
    }
	*/
   try
    {
         obj.addEventListener(event,fn,false);
    }
    catch(err) // for bastard IE
    {
         if(obj["on"+event] == null)
              obj["on"+event] = fn;
         else
         {
          var e = obj["on"+event];
          obj["on"+event] = function(){e(); fn();}
         }
    }	
}

//http://caniuse.com/#search=forEach
if (!('forEach' in Array.prototype)) {
    Array.prototype.forEach= function(action, that /*opt*/) {
        for (var i= 0, n= this.length; i<n; i++)
            if (i in this)
                action.call(that, this[i], i, this);
    };
}
/*
function hasClass(ele, cls) {
	return ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
}

function addClass(ele, cls) {
	if (!hasClass(ele, cls)) {
		ele.className += ' ' + cls;
	}
}

function removeClass(ele, cls) {
	if (hasClass(ele, cls)) {
		var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)'),
		newClass = ele.className.replace(reg, ' ');
		ele.className = newClass.replace(/^\s+|\s+$/g, '');
	}
}
*/

/*
function hasClass(el, clsName){
	var regex = new RegExp("(^|\\s)" + clsName + "(\\s|$)");
	return regex.test(el.className);
}

function addClass(el, clsName) {
	if (!hasClass(el,clsName)) {
		el.className += " "+ clsName;
	}
}

function removeClass (el, clsName) {
	var regex = new RegExp("(^|\\s)" + clsName + "(\\s|$)");
	el.className = el.className.replace(regex, " ");
}

function toggleClass(ele, cls) {
    var newClass = ' ' + ele.className.replace( /[\t\r\n]/g, ' ' ) + ' ';
    if (hasClass(ele, cls)) {
        while (newClass.indexOf(' ' + cls + ' ') >= 0 ) {
            newClass = newClass.replace( ' ' + cls + ' ' , ' ' );
        }
        ele.className = newClass.replace(/^\s+|\s+$/g, '');
    } else {
        ele.className += ' ' + cls;
    }
}
*/

function hasClass(el, cls) {
   return new RegExp('(\\s|^)'+cls+'(\\s|$)').test(el.className);
}
 
function addClass(el, cls)
{
   if (!hasClass(el, cls)) { el.className += (el.className ? ' ' : '') +cls; }
}
 
function removeClass(el, cls)
{
   if (hasClass(el, cls)) {
      el.className=el.className.replace(new RegExp('(\\s|^)'+cls+'(\\s|$)'),' ').replace(/^\s+|\s+$/g, '');
   }
}

function toggleClass(el, cls) {
    var newClass = ' ' + el.className.replace( /[\t\r\n]/g, ' ' ) + ' ';
    if (hasClass(el, cls)) {
        while (newClass.indexOf(' ' + cls + ' ') >= 0 ) {
            newClass = newClass.replace( ' ' + cls + ' ' , ' ' );
        }
        el.className = newClass.replace(/^\s+|\s+$/g, '');
    } else {
        el.className += ' ' + cls;
    }
}

/*
function hasClass(elem, className) {
    return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
}
function addClass(elem, className) {
    if (!hasClass(elem, className)) {
        elem.className += ' ' + className;
    }
}
function removeClass(elem, className) {
    var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ') + ' ';
    if (hasClass(elem, className)) {
        while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
            newClass = newClass.replace(' ' + className + ' ', ' ');
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    }
}
function toggleClass(elem, className) {
    var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ' ) + ' ';
    if (hasClass(elem, className)) {
        while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
            newClass = newClass.replace( ' ' + className + ' ' , ' ' );
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    } else {
        elem.className += ' ' + className;
    }
}
*/

function changeBtnColor(btn, cls ,time) {
	if (hasClass(btn,cls)) { return false; }
	addClass(btn,cls);
	setTimeout(function() {
		removeClass(btn,cls);
	},time);
}

function flushThis(id){
   var msie = 'Microsoft Internet Explorer';
   var tmp = 0;
   //var obj = document.getElementById(id);
   var obj = q(id);
   if (navigator.appName == msie){
      //tmp = obj.parentNode.offsetTop  +  'px';
	  
	  obj.scrollTop = obj.scrollTop - 1;
	  obj.scrollTop = obj.scrollTop + 1;
	  
   }else{
      tmp = obj.offsetTop;
   }
}
			
function addRow(tableID,fila) {
	// Get a reference to the table
	var tableRef = d(tableID).tBodies[0];
	var lastRow = tableRef.rows.length;
	
	// Insert a row in the table at row index lastRow
	var newRow   = tableRef.insertRow(lastRow);

	// Insert a cell in the row at index 0
	for (var c = 0; c < fila.length; c++) {
		var newCell  = newRow.insertCell(c);
		newCell.innerHTML = fila[c];
	}
}
			
function removeRows(tableID) {
	var tableRef = d(tableID).tBodies[0];
				
	var rowCount = tableRef.rows.length;
				
	//for (var r=rowCount-1; r>0; r--) {
	for (var r=rowCount-1; r>=0; r--) {
		tableRef.deleteRow(r);
	}
}

function serializeForm(form) {
  //form = document.getElementById(form) || document.forms[0];
  form = document.getElementById(form);
  var elems = form.elements;
  
  var serialized = [], i, len = elems.length, str='';
  
  for(i = 0; i < len; i += 1) {
  
    var element = elems[i];
    var type = element.type;
    var name = element.name;
    var value = element.value;
    
	if ((!element.disabled)&&(!element.readOnly)){
		switch(type) {

		case 'checkbox':
			if (!element.checked){
				break;
			}
		case 'text':
		case 'radio':
		case 'textarea':
		case 'password':
		case 'select-one':
			//str = name + '=' + value;
			//http://en.wikipedia.org/wiki/Percent-encoding
			//http://stackoverflow.com/questions/332872/how-to-encode-a-url-in-javascript
			str = name + '=' + encodeURIComponent(value);
			serialized.push(str);
			break;
		default:
			break;
		}    
	}
  }
  
  return serialized.join('&');

}

var PERIODO = 6000;
var TIMEOUT = 5000;

//var session;

addListener(window, 'load', function() {

	var stop = new Date();
	
	var mul = 1 + parseInt( (stop - start)/20000 );
	
	PERIODO = PERIODO * mul;
	TIMEOUT = TIMEOUT * mul;
	
	/*
	if ((stop - start) > 20000){
		var PERIODO = 12000;
		var TIMEOUT = 10000;	
	}
	else{
		var PERIODO = 6000;
		var TIMEOUT = 5000;	
	}
	*/
});

var p_load; 
var fn_load;
var clock = 0;

//Circulo Ajax
fn_load = function(){
	clock++;
	//if (clock > 9){
	if (clock > 11){
		clock = 0;
	}
	//removeClass(q('.loader li.playhead'),'playhead');
	Array.prototype.forEach.call(qa("#ajax-loader li"), function(li,i) {
		if (clock == i){
			addClass(li, 'playhead');
		}
		else{
			removeClass(li, 'playhead');
		}
	});
};
				
function showLoader(el) {
	var pos = { 
		left: el.offsetLeft, 
		top: el.offsetTop
	};

	pos.left = pos.left + (el.scrollWidth - 179)/2;
	//pos.top = pos.top + (el.scrollHeight - 179)/2;
	pos.top = pos.top + 75;
	//
	if(pos !== undefined) {
		d('ajax-loader').style['position'] = 'absolute';
		d('ajax-loader').style['left'] = pos.left + 'px';
		d('ajax-loader').style['top'] = pos.top + 'px';
	} else {
		d('ajax-loader').style['position'] = 'fixed';
		d('ajax-loader').style['top'] = '0px';
	}

	d('ajax-loader').style.display = '';
	
	p_load = setInterval(function(){fn_load()}, 125);
}

function hideLoader() {
	clearInterval(p_load);
	p_load = null;
	d('ajax-loader').style.display = 'none';
}

var p_ajax; 
var fn_ajax;
var clk = 0;
var clk_a = 0;
var cl;
var error = 0;

fn_ajax = function(){
	//removeClass(q('#s_ajax li.playhead'),'playhead');
	Array.prototype.forEach.call(qa("#s_ajax li"), function(li,i) {
	
		removeClass(li, 'r');
		removeClass(li, 'g');
		removeClass(li, 'playhead');
			
		if (i <= clk){
			if (i <= clk_a){
				if (error == 2){
					addClass(li,'r playhead');
				}
				else{
					addClass(li,'g playhead');
				}
				//addClass(li, cl + ' playhead');
			}
			else{
				addClass(li, 'playhead');
			}
		}
		//else{
			//removeClass(li, 'r');
			//removeClass(li, 'g');
			//removeClass(li, 'playhead');
		//}
	});
	
	clk++;
	if (error == 0) 
		clk_a++;
	
	if (clk > 12){
		clk = 0;
		clk_a = clk;
		//clk_a = 10;
		cl = 'g';
		//Array.prototype.forEach.call(qa("#s_ajax li"), function(li,i) {
		//	addClass(li, 'r playhead');
		//});
		//return;
	}
};

function showAjax() {
	//d('s_ajax').style.display = '';
	//d('s_load').style.display = 'none';
	removeClass(d('s_ajax'), 'hide');
	addClass(d('s_load'), 'hide');
	//clk = 9;

	error = 0;	
	clk = 0;
	clk_a = clk;
	cl = 'g';
	if (p_ajax == null){
		fn_ajax();
		p_ajax = setInterval(function(){fn_ajax()}, PERIODO/10);
	}
	
}

function showSuccess() {
	removeClass(d('s_ajax'), 'hide');
	addClass(d('s_load'), 'hide');
	error = 1;
	clk_a = clk;
	cl = 'g';
}

function showError() {
	removeClass(d('s_ajax'), 'hide');
	addClass(d('s_load'), 'hide');
	error = 2;
	clk_a = clk;
	cl = 'r';
	//d('s_ajax').style.display = 'none';
	//d('s_load').style.display = '';
}

function hideAjax() {
	if(p_ajax != null){
		clearInterval(p_ajax);
		p_ajax = null;
	}
	addClass(d('s_ajax'), 'hide');
	addClass(d('s_load'), 'hide');	
	//d('s_ajax').style.display = 'none';
	//d('s_load').style.display = 'none';
}
			
function getXMLValue(xmlData, field) {
	try {
		if(xmlData.getElementsByTagName(field)[0].firstChild.nodeValue)
			return xmlData.getElementsByTagName(field)[0].firstChild.nodeValue;
		else
			return null;
	} catch(err) { return null; }
}


function getAXMLValue(xmlData, field, att) {
	try {
		if(xmlData.getElementsByTagName(field)[0].attributes.getNamedItem(att).nodeValue)
			return xmlData.getElementsByTagName(field)[0].attributes.getNamedItem(att).nodeValue;
		else
			return null;
	} catch(err) { return null; }
}

var fontchecker_a;
var fontchecker_b;

function create_span(){
	fontchecker_a=document.createElement('span');
	(document.body).appendChild(fontchecker_a);
	fontchecker_a.style.fontFamily="Arial,monospace";
	fontchecker_a.style.margin="0px";
	fontchecker_a.style.padding="0px";
	fontchecker_a.style.fontSize="32px";
	fontchecker_a.style.position="absolute";
	fontchecker_a.style.top="-999px";
	fontchecker_a.style.left="-999px";
	fontchecker_a.innerHTML="Font Checker SPAN-A";
	fontchecker_b=document.createElement('span');
	(document.body).appendChild(fontchecker_b);
	fontchecker_b.style.fontFamily="Arial,monospace";
	fontchecker_b.style.margin="0px";
	fontchecker_b.style.padding="0px";
	fontchecker_b.style.fontSize="32px";
	fontchecker_b.style.position="absolute";
	fontchecker_b.style.top="-999px";
	fontchecker_b.style.left="-999px";
	fontchecker_b.innerHTML="Font Checker SPAN-B";
}

function checkfont(font){
	create_span();
	var txt = "ERROR",
		reg = /[\,\.\/\;\'\[\]\`\<\>\\\?\:\"\{\}\|\~\!\@\#\$\%\^\&\*\(\)\-\=\_\+ ]/g;
	//font = font.replace(reg,"");
	fontchecker_a.style.fontFamily=font+",monospace";
	fontchecker_b.style.fontFamily="monospace";
	fontchecker_a.innerHTML="random_words_#_!@#$^&*()_+mdvejreu_RANDOM_WORDS";
	fontchecker_b.innerHTML=fontchecker_a.innerHTML;
	if(parseInt(fontchecker_a.offsetWidth,10) == parseInt(fontchecker_b.offsetWidth,10)&&Number(fontchecker_a.offsetHeight)==Number(fontchecker_b.offsetHeight)){
		fontchecker_a.style.fontFamily=font+",Arial";
		fontchecker_b.style.fontFamily="Arial";
		if(Number(fontchecker_a.offsetWidth)==Number(fontchecker_b.offsetWidth)&&Number(fontchecker_a.offsetHeight)==Number(fontchecker_b.offsetHeight)){
			txt = false;
		}else{
			txt = true;
		}
	}else{
		txt = true;
	}
	//DELETE TESTING ELEMENTS
	fontchecker_a.innerHTML="";
	fontchecker_a.outerHTML="";
	fontchecker_b.innerHTML="";
	fontchecker_b.outerHTML="";
	
	return txt;
}


var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
						|| this.searchVersion(navigator.appVersion)
						|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
			}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera",
			versionSearch: "Version"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			string: navigator.userAgent,
			subString: "iPhone",
			identity: "iPhone/iPod"
		},
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]
};

function isIE () {
  var myNav = navigator.userAgent.toLowerCase();
  return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}
		
function getCookieVal (offset) {  
	var endstr = document.cookie.indexOf (";", offset);  
	if (endstr == -1)
		endstr = document.cookie.length;  
	return unescape(document.cookie.substring(offset, endstr));
}
			  
function GetCookie (name) {          
	var arg = name + "=";  
	var alen = arg.length;
	var clen = document.cookie.length;  
	var i = 0;
	while (i < clen) {
		var j = i + alen;    
		if (document.cookie.substring(i, j) == arg)      
			return getCookieVal (j);    
		i = document.cookie.indexOf(" ", i) + 1;    
		if (i == 0) break;
	}
	return null;
}
			  
function SetCookie (name, value) {
	var argv = SetCookie.arguments;  
	var argc = SetCookie.arguments.length;  
	var expires = (argc > 2) ? argv[2] : null;  
	var path = (argc > 3) ? argv[3] : null;  
	var domain = (argc > 4) ? argv[4] : null;  
	var secure = (argc > 5) ? argv[5] : false;
	//var httponly = true;				
	document.cookie = name + "=" + escape (value) + 
		((expires == null) ? "" : ("; expires=" + expires.toGMTString())) + 
		((path == null) ? "" : ("; path=" + path)) +  
		((domain == null) ? "" : ("; domain=" + domain)) +    
		((secure == true) ? "; secure" : "") ;
		//((httponly == true) ? "; httponly" : "");
}
			  
function DeleteCookie (name) {  
	var exp = new Date();
	exp.setTime (exp.getTime() - 1);
			  
	// This cookie is history
	var cval = GetCookie (name);  
	document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
}
		
function tinyxhr(url,cb,method,post,contenttype,timeout)
{
	var requestTimeout,xhr;
	/*
	try{ xhr = new XMLHttpRequest(); }
	catch(e){
		try{ xhr = new ActiveXObject("Msxml2.XMLHTTP"); }
			catch (e){
				if(console)console.log("tinyxhr: XMLHttpRequest not supported");
				return null;
			}
	}
	*/
	
	SessionRestart();
	
	if(window.XMLHttpRequest) {
		xhr = new XMLHttpRequest(); // native Fx and IE7
	} else {
		try {
			xhr = new ActiveXObject('MSXML2.XMLHTTP.6.0'); // latest ActiveX
		} catch (e) {
			try {
				xhr = new ActiveXObject('Microsoft.XMLHTTP'); // older ActiveX
			} catch (e) {
				xhr = false;
				if(console)console.log("tinyxhr: XMLHttpRequest not supported");
				return null;
			}
		}
	}
	
	if (timeout > 0){
		requestTimeout = setTimeout(function() {
									xhr.abort(); 
									cb(new Error("tinyxhr: aborted by a timeout"), "",xhr); }
                             , timeout);
	}
	else{
		requestTimeout = null;
	}
	
	xhr.onreadystatechange = function()
	{
		if (xhr.readyState != 4) return;
		if (requestTimeout != null){
			clearTimeout(requestTimeout);		
		}
		cb(xhr.status != 200?new Error("tinyxhr: server respnse status is "+xhr.status):false, xhr.responseText,xhr);
	};
	xhr.open(method?method.toUpperCase():"GET", url, true);
 
	if(!post)
		xhr.send();
	else
	{
		if (contenttype == 'multipart/form-data'){
			//http://mike.kaply.com/2010/05/20/post-multipart-form-xhr/
			//http://stackoverflow.com/questions/3690611/how-to-create-an-ajax-request-with-javascript-that-contains-both-file-and-post-d

			// Create form data
			var formData = new FormData();
			formData.append('file', post);
			
			var seconds_ini,seconds_act,filesize,speed = 0;
			filesize = post.size;
			seconds_ini = Math.floor(new Date().getTime() / 1000);
			
			if (post.name.match('\.bin')){
				//q('#loading').innerHTML = '';
				q('#bar_upload').style.width = '0%';
				q('#percent_upload').textContent = '0%';
			}
			else{
				//q('#cfg_loading').innerHTML = '';
				q('#cfg_bar').style.width = '0%';
				q('#cfg_percent').textContent = '0%';
			}
			
			//xhr.setRequestHeader("Authorization", "Basic " + btoa("ZIOR" + ":" + "4231")); 
			
			//if (xhr.upload)
			//	xhr.upload.addEventListener("progress", updateProgress, false);
			
			if (xhr.upload) {
				xhr.upload.onprogress = function(e) {
				
				//function updateProgress (e) {
				
					SessionRestart();
				
					var percentVal = 0;
					var position = e.loaded || e.position; /*e.position is deprecated*/
					var total = e.total || e.totalSize;
					if (e.lengthComputable) {
						//percent = Math.ceil(position / total * 100);
						percentVal =  Math.round((position / total) * 100);
					}
						
					seconds_act = Math.floor(new Date().getTime() / 1000);
					if (seconds_act != seconds_ini){
						speed = Math.floor(((filesize * percentVal) / 100)/(1024*(seconds_act - seconds_ini)));
					}
					
					if (post.name.match('\.bin')){
						q('#bar_upload').style.width = percentVal + '%';
						q('#percent_upload').textContent = percentVal + '%';
						q('#speed_upload').textContent = 'Tiempo:' + (seconds_act - seconds_ini) + 'sec' + '\t  Velocidad:' + (speed) + 'Kbytes/sec';
						//q('#loading').innerHTML = "Cargando...";
						q('#status_upload').innerHTML = "Subiendo Fichero...";
					}
					else{
						q('#cfg_bar').style.width = percentVal + '%';
						q('#cfg_percent').textContent = percentVal + '%';
						q('#cfg_speed').textContent = 'Tiempo:' + (seconds_act - seconds_ini) + 'sec' + '\t  Velocidad:' + (speed) + 'Kbytes/sec';
						//q('#cfg_loading').innerHTML = "Cargando...";
						q('#cfg_status').innerHTML = "Subiendo Fichero...";
					}
				};
			}
			xhr.send(formData);
		}
		else{
			xhr.setRequestHeader('Content-type', contenttype?contenttype:'application/x-www-form-urlencoded');
			xhr.send(post);
		}
	}
	//aibabe
	return xhr;
	//
}

function fileUpload(form, action_url, div_id) {
    // Create the iframe...
    var iframe = document.createElement("iframe");
    iframe.setAttribute("id", "upload_iframe");
    iframe.setAttribute("name", "upload_iframe");
    iframe.setAttribute("width", "0");
    iframe.setAttribute("height", "0");
    iframe.setAttribute("border", "0");
    iframe.setAttribute("style", "width: 0; height: 0; border: none;");
 
    // Add to document...
    form.parentNode.appendChild(iframe);
    window.frames['upload_iframe'].name = "upload_iframe";
 
    iframeId = document.getElementById("upload_iframe");
 
    // Add event...
    var eventHandler = function () {
 
            if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
            else iframeId.removeEventListener("load", eventHandler, false);
 
            // Message from server...
            if (iframeId.contentDocument) {
                content = iframeId.contentDocument.body.innerHTML;
            } else if (iframeId.contentWindow) {
                content = iframeId.contentWindow.document.body.innerHTML;
            } else if (iframeId.document) {
                content = iframeId.document.body.innerHTML;
            }
 
            document.getElementById(div_id).innerHTML = content;

			/*
			console.log("Upload Success");
			var percentVal = '100%';
			q('#bar_upload').style.width = percentVal;
			q('#percent_upload').textContent = percentVal;
			q('#loading').textContent = "Loaded";
			*/
			boot_ajax.restart();
			
			hideLoader();
			
            // Del the iframe...
            //setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
			setTimeout(function() {
							iframeId.parentNode.removeChild(iframeId);
						}, 250 );
	}
 
    if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
    if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);
 
    // Set properties of form...
    form.setAttribute("target", "upload_iframe");
    form.setAttribute("action", action_url);
    form.setAttribute("method", "post");
    form.setAttribute("enctype", "multipart/form-data");
    form.setAttribute("encoding", "multipart/form-data");
 
    // Submit the form...
    form.submit();
 
    document.getElementById(div_id).innerHTML = "<b>Uploading...</b>";
}

var parpadeo_leds = null;
var parpadeo_barra = null;
var flip1 = 0;
var flip2 = 0;
var leds;
var color;
var ROJO = '#9d261d';
var VERDE = '#1d9d26';
var BLANCO = '#ffffff';
var GRIS = '#333';

var ielem = 0;
var index = 0;
var ielem_ant = 0;
var index_ant = 0;
			
//var svgDoc = null;
var na,nh,nr,ni,nw,ne,ng,nb;
var mr,mo,ml,me;
			
var selectedId;
var dashboard_ajax;
var xhr_dashboard;
var events_ajax;
var xhr_events;
var status_ajax;
var xhr_status;
var settings_ajax;
var xhr_settings;
var settings_his_ajax;
var xhr_settings_his;
var control_ajax;
var xhr_control;
var alarms_ajax;
var xhr_alarms;
var alarm_settings_ajax;
var xhr_alarm_settings;
var network_ajax;
var xhr_network;
var equipo_ajax;
var xhr_equipo;
var boot_ajax;
var xhr_boot;
			
//var PERIODO = 6000;
//var TIMEOUT = 5000;
			
var originalHash;
			
// Parses the xmlResponse from status.xml and updates the status box
function updateDashboard(xmlData) {

	if(!xmlData)
	{
		d("vrecv").textContent = '?';
		d("irecv").textContent = '?';
		d("ibatv").textContent = '?';
		d("regimenv").textContent = '?';
		return;
	}
	
	d("vrecv").textContent = getXMLValue(xmlData,'vrec');
	d("irecv").textContent = getXMLValue(xmlData,'irec');
	d("ibatv").textContent = getXMLValue(xmlData,'ibat');
	d("regimenv").textContent = getXMLValue(xmlData,'regimen');
	
	if (d("ibatv").textContent == "SIN BATERIA"){
		d('batg').style.display = 'none';
		d('bat_out').style.display = 'none';
		
		//d('batt').setAttribute('x',378.99408);
		//d('batt').setAttribute('transform', 'translate(578 462)');
	}
	else{
		d('batg').style.display = '';
		d('bat_out').style.display = '';
		
		//d('batt').setAttribute('x',778.99408);
		//d('batt').setAttribute('transform', 'translate(779 462)');
	}
	
	//reles
	var reles = parseInt(getXMLValue(xmlData,'reles'), 2);
	
	if (reles & 0x01){
		color = ROJO;
	}
	else{
		color = BLANCO;
	}
	d('rele1').setAttribute('fill',color);

	if (reles & 0x02){
		color = ROJO;
	}
	else{
		color = BLANCO;
	}
	d('rele2').setAttribute('fill',color);
	
	
	if (reles & 0x04){
		color = ROJO;
	}
	else{
		color = BLANCO;
	}
	d('rele3').setAttribute('fill',color);
	
	if (reles & 0x08){
		color = ROJO;
	}
	else{
		color = BLANCO;
	}
	d('rele4').setAttribute('fill',color);
	
	//leds
	leds = parseInt(getXMLValue(xmlData,'leds'), 16);
	
	if (((leds & 0xF00) > 0x200)||((leds & 0x0F0) > 0x020)||((leds & 0x00F) > 0x002)){
		if(parpadeo_leds == null){
			parpadeo_leds = setInterval( function() {
				flip1++;
				
				if ((leds & 0xF00) >= 0x300){
					if (flip1 % 2 == 0){
						if ((leds & 0xF00) == 0x400){
							color = ROJO;
						}
						else{
							color = VERDE;									
						}
					}
					else{
						color = BLANCO;
					}
					d('led1').setAttribute('fill',color);
				}
				
				if ((leds & 0x0F0) >= 0x030){
					if (flip1 % 2 == 0){
						if ((leds & 0x0F0) == 0x040){
							color = ROJO;
						}
						else{
							color = VERDE;									
						}
					}
					else{
						color = BLANCO;
					}
					d('led2').setAttribute('fill',color);
				}
				
				if ((leds & 0x00F) >= 0x003){
					if (flip1 % 2 == 0){
						if ((leds & 0x00F) == 0x004){
							color = ROJO;
						}
						else{
							color = VERDE;									
						}
					}
					else{
						color = BLANCO;
					}
					d('led3').setAttribute('fill',color);
				}
			},500);
		}
	}
	else{
		if(parpadeo_leds != null){
			clearInterval(parpadeo_leds);
			parpadeo_leds = null;
		}
	}
	
	//led1
	if ((leds & 0xF00) == 0x100){//verde
		d('led1').setAttribute('fill',VERDE);					
	}
	else if ((leds & 0xF00) == 0x200){//rojo
		d('led1').setAttribute('fill',ROJO);
	}
	else if ((leds & 0xF00) == 0x000){
		d('led1').setAttribute('fill',BLANCO);
	}
	
	//led2
	if ((leds & 0x0F0) == 0x010){//verde
		d('led2').setAttribute('fill',VERDE);					
	}
	else if ((leds & 0x0F0) == 0x020){//rojo
		d('led2').setAttribute('fill',ROJO);
	}
	else if ((leds & 0x0F0) == 0x000){
		d('led2').setAttribute('fill',BLANCO);
	}
	
	//led3
	if ((leds & 0x00F) == 0x001){//verde
		d('led3').setAttribute('fill',VERDE);					
	}
	else if ((leds & 0x00F) == 0x002){//rojo
		d('led3').setAttribute('fill',ROJO);
	}
	else if ((leds & 0x00F) == 0x000){
		d('led3').setAttribute('fill',BLANCO);
	}

	//Flujo
	var rectificador = 0;
	if (parseInt( getXMLValue(xmlData,'ac') ) != 1 ){
		d('enchufe').setAttribute('fill',VERDE);
		d('rec_in').setAttribute('stroke',VERDE);

		if (parseInt( getXMLValue(xmlData,'marcha') ) != 0 ){					
			d('rectifier').setAttribute('stroke',VERDE);
			d('rec_out').setAttribute('stroke',VERDE);
			
			rectificador = 1;
		}
		else{
			d('rectifier').setAttribute('stroke',GRIS);
			d('rec_out').setAttribute('stroke',GRIS);
		}
	}
	else{
		d('enchufe').setAttribute('fill',GRIS);
		d('rec_in').setAttribute('stroke',GRIS);
		d('rectifier').setAttribute('stroke',GRIS);
		d('rec_out').setAttribute('stroke',GRIS);					
	}
	
	var bateria = 0;

	if ((parseInt( getXMLValue(xmlData,'LVBD') ) == 1 )&&
		(parseInt( getXMLValue(xmlData,'fusible') ) == 1)){
		
		d('mag_ent').setAttribute('d','m 157.67,170.14 59.896,0');
		
		d('bat_in').setAttribute('stroke',VERDE);
		d('mag_ent_g').setAttribute('stroke',VERDE);
		d('bat_in-1').setAttribute('stroke',VERDE);
		d('battery').setAttribute('fill',VERDE);
		d('bat_out').setAttribute('stroke',VERDE);
		
		bateria = 1;
	}
	else{
		d('mag_ent').setAttribute('d','m 157.67,170.14 59.896,-30');
		
		d('bat_in').setAttribute('stroke',GRIS);
		d('mag_ent_g').setAttribute('stroke',GRIS);
		d('bat_in-1').setAttribute('stroke',GRIS);
		d('battery').setAttribute('fill',GRIS);
		d('bat_out').setAttribute('stroke',GRIS);
	}
	
	
	if ( (rectificador == 1) ||
		 (bateria == 1) ){
		d('int_sal_out').setAttribute('stroke',VERDE);
		d('salida').setAttribute('fill',VERDE);
	}
	else{
		d('int_sal_out').setAttribute('stroke',GRIS);
		d('salida').setAttribute('fill',GRIS);
	}
	
	//parpadeo_barra
	if (parseFloat( getXMLValue(xmlData,'ibat') ) > 0){
		if(parpadeo_barra == null){
			parpadeo_barra = setInterval( function() {
				if (flip2++ % 2 == 0){
					d('Tximist').style.display = 'none';
				}
				else{
					d('Tximist').style.display = '';
				}
			},500);
		}
	}
	else{
		if(parpadeo_barra != null){
			clearInterval(parpadeo_barra);
			parpadeo_barra = null;
		}
		//$('#Tximist',svgDoc).show();
		d('Tximist').style.display = '';
	}
}
	

// Parses the xmlResponse from alarms.xml and updates the status box
function updateEvents(xmlData) {
	if(!xmlData)
	{
		return;
	}

	d('ubicacion').innerHTML = '<span class="uni">&#x1f30f; </span>' + getXMLValue(xmlData,'loc');
	
	var tact = parseInt( getXMLValue(xmlData,'tact') );
	
	if ((tact > 60)||(tact <= 0)){
		tact = 10;	
	}
	
	//if (tact != (Math.floor(SessionTimeleft/tickDuration/60))){
	//if ((tact > 0)&&(tact != (Math.floor(SessionTimeleft/tickDuration/60)))){
		SessionTime = tact * 60 * tickDuration;
		SessionRestart();
	//}
	
	//na = parseInt( getXMLValue(xmlData,'na') );
	
	if (na != parseInt( getXMLValue(xmlData,'na') )){
		na = parseInt( getXMLValue(xmlData,'na') );
		
		if (!hasClass(qa("#alarms_id")[0], 'hide')){
			if (!hasClass(qa("#alarms_act_tab")[0], 'hide')){
				var myeve = document.createEvent("HTMLEvents");
				myeve.initEvent("click", true, true);
				q('#alarms_id .navs a[href="#alarms_act_tab"]').dispatchEvent(myeve);
			}
		}
	}
	
	nh = parseInt( getXMLValue(xmlData,'nh') );
	nr = parseInt( getXMLValue(xmlData,'nr') );
	ni = parseInt( getXMLValue(xmlData,'ni') );
	nw = parseInt( getXMLValue(xmlData,'nw') );
	ne = parseInt( getXMLValue(xmlData,'ne') );
	ng = parseInt( getXMLValue(xmlData,'ng') );
	nb = parseInt( getXMLValue(xmlData,'nb') );
	
	mr = parseInt( getXMLValue(xmlData,'mr') );
	mo = parseInt( getXMLValue(xmlData,'mo') );
	ml = parseInt( getXMLValue(xmlData,'ml') );
	me = parseInt( getXMLValue(xmlData,'me') );
	
	if (na > 0){
		d('alarm-badge1').style.display = '';
		d('alarm-badge1').textContent = na.toString();
	}
	else{
		d('alarm-badge1').style.display = 'none';
	}
	
	d('active-badge').textContent = na.toString();
	d('history-badge').textContent = nh.toString();
	d('cfghis-badge').textContent = mr.toString();
	
	d('active-notif').textContent = na.toString();
	d('pending-notif').textContent = nr.toString();
	d('ack-notif').textContent = (na - nr).toString();
	
	d('his-notif').textContent = nh.toString();
	d('alarm-notif').textContent = ne.toString();
	d('warning-notif').textContent = nw.toString();
	d('red-notif').textContent = ng.toString();
	d('alta-notif').textContent = nb.toString();
	d('info-notif').textContent = ni.toString();
	
	d('cfghis-notif').textContent = mr.toString();
	d('ordenhis-notif').textContent = mo.toString();
	d('paramhis-notif').textContent = me.toString();
	d('loginhis-notif').textContent = ml.toString();
}
			
/*<!-- Dashboard-->*/

//$(document).ready(function(){
//$(window).load(function (){
addListener(window, 'load', function() {

	var stop = new Date();
	//d("ms").innerHTML          = (stop - start);
	
	dashboard_ajax = periodic({period: PERIODO, decay: 1, max_period: 60000}, function() {
		if(xhr_dashboard && xhr_dashboard.readystate != 4){
			xhr_dashboard.abort();
		}
		
		//d('dashboard_load').innerHTML = '<span class="uni">&#x21ba;</span> Cargando...';
		//d('status_load').innerHTML = 'Cargando...';
		showAjax();
		
		xhr_dashboard = tinyxhr('dashboard.xml',
			function (err,data,xhr){ 
				if (err){
					console.log("goterr ",err,'status='+xhr.status);
					//d('dashboard_load').innerHTML = "<span class='uni'>&#9888;</span> Fallo COM";
					//d('status_load').innerHTML = "<span class='uni'>&#9888;</span> Fallo COM";
					showError();
				}
				else{
					if (!d("AjaxCheckbox").checked){
						//d('dashboard_load').innerHTML = " ";
						//d('status_load').innerHTML = " ";
						hideAjax();
					}
					else{
						showSuccess();
					}
					updateDashboard(xhr.responseXML);
				}

				if (!d("AjaxCheckbox").checked){
					dashboard_ajax.cancel();
				}
			},
			"GET",
			null,
			"xml",
			TIMEOUT	// in milliseconds
		);
	});
	
	events_ajax = periodic({period: PERIODO, decay: 1, max_period: 60000}, function() {
		if(xhr_events && xhr_events.readystate != 4){
			xhr_events.abort();
		}
		xhr_events = tinyxhr('alarms.xml',
			function (err,data,xhr){ 
				if (err){
					console.log("goterr ",err,'status='+xhr.status);
				}
				else{
					updateEvents(xhr.responseXML);
				}
				//
				if (!d("AjaxCheckbox").checked){
					events_ajax.cancel();
				}
				//
			},
			"GET",
			null,
			"xml",
			TIMEOUT	// in milliseconds
		);
	});
});


/*<!-- Status-->*/

function updateStatus(htmlData) {
	if(!htmlData)
	{
		return;
	}
	removeRows('status_table');
	var jsonData= eval(htmlData);
	for(var i = 0; i < (jsonData.aaData.length); i++) {
		var fila = new Array();
		fila = jsonData.aaData[i];
		addRow('status_table',fila);
	}
	ielem_ant = ielem;
	index_ant = index;
}
//$(document).ready(function() {
addListener(window, 'load', function() {

	status_ajax = periodic({period: PERIODO, decay: 1, max_period: 60000}, function() {
		if(xhr_status && xhr_status.readystate != 4){
			xhr_status.abort();
		}
		
		//d('status_load').innerHTML = 'Cargando...';
		showAjax();
		
		xhr_status = tinyxhr('status.cgi?ielem=' + ielem + '&index=' + index,
			function (err,data,xhr){ 
				if (err){
					console.log("goterr ",err,'status='+xhr.status);
					//d('status_load').innerHTML = "<span class='uni'>&#9888;</span> Fallo COM";
					showError();
				}
				else{
					if (!d("AjaxCheckbox").checked){
						//d('status_load').innerHTML = " ";
						hideAjax();
					}
					else{
						showSuccess();
					}
					updateStatus(data);
				}

				if (!d("AjaxCheckbox").checked){
					status_ajax.cancel();
				}
				
				hideLoader();
				d('status_table').tBodies[0].style.display = '';
			},
			"GET",
			null,
			"html",
			TIMEOUT	// in milliseconds
		);
	});

	d('AjaxCheckbox').onclick = function() { 
		if (d("AjaxCheckbox").checked){
			//
			events_ajax.restart();
			//
			if (selectedId == '#dashboard_id'){
				dashboard_ajax.restart();
			}
			else if (selectedId == '#status_id'){
				status_ajax.restart();
			}
			else if(selectedId == '#settings_id'){
				settings_ajax.restart();
			}
			else if(selectedId == '#alarms_id'){
				alarms_ajax.restart();
			}
			else if(selectedId == '#control_id'){
				control_ajax.restart();
			}
			else if(selectedId == '#network_id'){
				network_ajax.restart();
			}
			else if(selectedId == '#equipo_id'){
				equipo_ajax.restart();
			}
			else if(selectedId == '#boot_id'){
				boot_ajax.restart();
			}
		} else {
			//
			events_ajax.cancel();
			//
			dashboard_ajax.cancel();
			status_ajax.cancel();
			settings_ajax.cancel();
			alarms_ajax.cancel();
			control_ajax.cancel();
			network_ajax.cancel();
			equipo_ajax.cancel();
			boot_ajax.cancel();
			//d('status_load').innerHTML = " ";
			hideAjax();
		} 
	};
	
	Array.prototype.forEach.call(qa(".status_tree li a"), function(el) {
		el.onclick = function(e) {
		
		//
		var e = e || window.event; // IE compatibility
		(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
		//
	
		//$('.status_tree li.active').removeClass('active');
		var tli = this;
		Array.prototype.forEach.call(qa(".status_tree li a"), function(a,i) {
			removeClass(qa(".status_tree li").item(i),'selected');
			if (qa(".status_tree li a").item(i) == tli){
				ielem = i%5;
			}
		});
		
		//removeClass(q('.status_tree li.selected'),'selected');
		//this.parentNode.className += 'selected';
		
		addClass(qa(".status_tree li").item(ielem),'selected');
		addClass(qa(".status_tree li").item(ielem + 5),'selected');
		
		var scroll = q('#status_id .scrollcontainer');
		if (ielem >= 1){
			scroll.scrollLeft = ((scroll.scrollWidth - scroll.clientWidth) * (ielem - 1)) / 4;
		}
		else{
			scroll.scrollLeft = 0;		
		}
		
		
		//ielem = $('.status_tree li a').index(this);
		//ielem = n;
		index = 0;
		
		originalHash = this.hash.split('_li')[0];
		location.hash = this.hash.split('_li')[0];
		
		qa('.breadcrumb span')[2].textContent = " › " + this.lastChild.nodeValue;
		qa('.breadcrumb span')[3].textContent = '';

		status_ajax.restart();
		//
		events_ajax.restart();
		//
		
		showLoader(d('status_table'));
		
		d('status_table').tBodies[0].style.display = 'none';

		return false;
		};
	});
});

/*<!-- Settings-->*/

function updateSettings(htmlData) {
	if(!htmlData)
	{
		return;
	}

	removeRows('settings_table');
	
	var jsonData= eval(htmlData);
	
	for(var i = 0; i < (jsonData.aaData.length); i++) {
		var fila = new Array();
		fila = jsonData.aaData[i];
		var iElem = parseInt(jsonData.aaData[i][1][0]);
		var indice = parseInt(jsonData.aaData[i][1][1]);
		var ivar = parseInt(jsonData.aaData[i][1][2]);
		var valor = jsonData.aaData[i][1][3];
		
		var select;
		if (jsonData.aaData[i][1][4]){
			select = "<select name='value' disabled>"; 
			for(var j = 0; j < (jsonData.aaData[i][1][4].length); j++) {
				if (jsonData.aaData[i][1][4][j] == valor){
					select = select + "<option value='" + j + "' selected='true'>" + jsonData.aaData[i][1][4][j] + "</option>";
				}
				else{
					select = select + "<option value='" + j + "'>" + jsonData.aaData[i][1][4][j] + "</option>";
				}
			}	
			select = select + "</select>";
		}
		else{
			select = "<input readonly type='text' name='value' value='" + valor + "' />";
		}

		fila[1] = "<form id='form-simple'>" +
						"<input type='hidden' name='ielem' value='" + iElem + "'/>" +
						"<input type='hidden' name='index' value='" + indice + "'/>" +
						"<input type='hidden' name='ivar' value='" + ivar + "'/>" +
						"<span>" + select + "</span>" +
					"</form>";
		
		if (parseInt(jsonData.aaData[i][4])){
			//fila[4] = "<a href='javascript:void(0)'><b style='font-size: 20px' class='uni'>&#x270e;*</b><span style='display:none'>1</span></a>";
			fila[4] = "<a href='javascript:void(0)'><b style='font-size: 20px' class='edit'>&#x270e;*</b><span style='display:none'>1</span></a>";
		}
		else{
			fila[4] = "<span style='display:none'>0</span>";
		}
		
		addRow('settings_table',fila);
	}
	
	ielem_ant = ielem;
	index_ant = index;
}

function updateSettingsHis(htmlData) {

	if(!htmlData)
	{
		return;
	}
	
	//removeRows('settings_his_table');
	//var jsonData= eval( "(" + htmlData + ")" );
	//var sp = htmlData.split(/<\s*tr[^>]*>(.*?)<\s*\/\s*tr>/g);
	var sp = htmlData.split(/(?:<tr>|<\/tr>)/ig);
	var filas = [];
	for (var i=0;i<sp.length;i++){
		//if ((i+2)%2==1) {
		if (sp[i] != '') {
			filas.push(sp[i]);
		}
		//}
	}
	
	//for(var i = 0; i < (jsonData.aaData.length); i++) {
	for(var j = 0; j < (filas.length); j++) {
		//sp = filas[j].split(/<\s*td[^>]*>(.*?)<\s*\/\s*td>/g);
		sp = filas[j].split(/(?:<td>|<\/td>)/ig);
		var fila = [];
		for (var i=0;i<sp.length;i++){
			if ((i%2)==1) {
			//if (sp[i] != '') {
				fila.push(sp[i]);
			}
		}
		
		addRow('settings_his_table',fila);
	}
	
	var total = mr;
	
	index = index + filas.length;
	if (index > total){
		index = total;
	}
	var percentVal = Math.floor((index)/total*100) + '%';
	d('settings_his_bar').style.width = percentVal;
	d('settings_his_bar').parentNode.style.display = '';
	d('settings_his_bar').innerHTML = percentVal;
	
	if (index < total){
		//index = index + jsonData.aaData.length;
		settings_his_ajax.restart();
	}
	else{
		if (ielem == 31){
			setTimeout(function() {
				d('settings_his_bar').parentNode.style.display = 'none';
			}, 3000 );
		}
		settings_his_ajax.cancel();
	}
}

function isValidDate(sText) {
  //var reDate = /(?:0[1-9]|[12][0-9]|3[01])\/(?:0[1-9]|1[0-2])\/(?:19|20\d{2})/;
  var regDate = /^(\d{2}):(\d{2}) (\d{2})\/(\d{2})\/(\d{2})$/;
  
  if (regDate.test(sText)){
	
	sText = sText.replace(":", "/").replace(" ", "/").replace("-", "/").replace(".", "/"); 
	var SplitValue = sText.split("/");
	
	var Hour = parseInt(SplitValue[0],10);
	var Min = parseInt(SplitValue[1],10);
		
	var Day = parseInt(SplitValue[2],10);
	var Month = parseInt(SplitValue[3],10);
	var Year = parseInt(SplitValue[4],10) + 2000;
	
	var OK = true;
	if (OK = ((Year >= 2000) && (Year <= 2100))) {
		if (OK = (Month <= 12 && Month > 0)) {
		var LeapYear = (((Year % 4) == 0) && ((Year % 100) != 0) || ((Year % 400) == 0));
 
	   if (Month == 2) {
		   OK = LeapYear ? Day <= 29 : Day <= 28;
	   }
	   else {
		 if ((Month == 4) || (Month == 6) || (Month == 9) || (Month == 11)) {
		   OK = (Day > 0 && Day <= 30);
		 }
		 else {
			 OK = (Day > 0 && Day <= 31);
		 }
			 }
			}
		}
		return OK;
  }
  else{
	return false;
  }
}

function updateTips( tips, t ) {
	//var tips = q('#Tips');
	tips.innerHTML = t;
	addClass(tips, "r");
		
	setTimeout(function() {
		removeClass(tips,'r');
	}, 3000 );
}

function checkValue( o, min, max ) {
	if ( ( o > max) || (o < min) || (isNaN(o)) ){
		return false;
	} else {
		return true;
	}
}

//$(document).ready(function() {
addListener(window, 'load', function() {

	settings_ajax = periodic({period: PERIODO, decay: 1, max_period: 60000}, function() {
		if(xhr_settings && xhr_settings.readystate != 4){
			xhr_settings.abort();
		}
		
		//d('status_load').innerHTML = 'Cargando...';
		showAjax();
		
		xhr_settings = tinyxhr('settings.cgi?ielem=' + ielem + '&index=' + index,
			function (err,data,xhr){ 
				if (err){
					console.log("goterr ",err,'status='+xhr.status);
					//d('settings_load').getElementsByTagName('small')[0].innerHTML = "<span class='uni'>&#9888;</span> Fallo COM";
					//d('status_load').innerHTML = "<span class='uni'>&#9888;</span> Fallo COM";
					showError();
				}
				else{
					//if (!d("settingsAjaxCheckbox").checked){
						//d('settings_load').getElementsByTagName('small')[0].innerHTML = " ";
						//d('status_load').innerHTML = " ";
						hideAjax();
					//}
					updateSettings(data);
					d('settings_table').tBodies[0].style.display = '';
				}

				//if (!d("settingsAjaxCheckbox").checked){
					//this.periodic.cancel();
					settings_ajax.cancel();
				
					//d('status_load').getElementsByTagName('small')[0].innerHTML = " ";
					//fadeOut(750,d('status_load'));
					//load_st.cancel();
				//}
				
				//$.hideLoader();
				hideLoader();
				//d('settings_table').tBodies[0].style.display = '';
			},
			"GET",
			null,
			"html",
			TIMEOUT	// in milliseconds
		);
	});

	settings_his_ajax = periodic({period: PERIODO, decay: 1, max_period: 60000}, function() {
		if(xhr_settings_his && xhr_settings_his.readystate != 4){
			xhr_settings_his.abort();
		}
		xhr_settings_his = tinyxhr('regsEdit.cgi?ielem=' + ielem + '&index=' + index,
			function (err,data,xhr){ 
			
				settings_his_ajax.cancel();
				
				if (err){
					console.log("Settings History Failed");
					console.log("goterr ",err,'status='+xhr.status);
					settings_his_ajax.restart();
				}
				else{
					console.log("Settings History Success");
					updateSettingsHis(data);
				}

				//settings_his_ajax.cancel();
			},
			"GET",
			null,
			"html",
			TIMEOUT	// in milliseconds
		);
	});
	
	Array.prototype.forEach.call(qa(".settings_tree li a"), function(el) {
		el.onclick = function(e) {
		
		//
		var e = e || window.event; // IE compatibility
		(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
		//
		
		var tli = this;
		Array.prototype.forEach.call(qa(".settings_tree li a"), function(a,i) {
		
			removeClass(qa(".settings_tree li").item(i),'selected');
		
			if (qa(".settings_tree li a").item(i) == tli){
				ielem = i%5;
				//addClass(qa(".settings_tree li").item(i),'selected');
			}
			
			//removeClass(qa(".settings_tree li").item(i),'selected');
		});
		
		//removeClass(q('.settings_tree li.selected'),'selected');
		
		addClass(qa(".settings_tree li").item(ielem),'selected');
		addClass(qa(".settings_tree li").item(ielem + 5),'selected');
		
		var scroll = q('#settings_id .scrollcontainer');
		//scroll.scrollLeft = ((scroll.scrollWidth - scroll.clientWidth) * ielem) / 4;
		if (ielem >= 1){
			scroll.scrollLeft = ((scroll.scrollWidth - scroll.clientWidth) * (ielem - 1)) / 4;
		}
		else{
			scroll.scrollLeft = 0;		
		}

		//ielem = n;
		index = 0;
		
		originalHash = this.hash.split('_li')[0];
		location.hash = this.hash.split('_li')[0];
		
		qa('.breadcrumb span')[3].textContent = " › " + this.lastChild.nodeValue;
		
		settings_ajax.restart();
		//
		events_ajax.restart();
		//
		
		showLoader(d('settings_table'));
		
		return false;
		};
	});
	
	//$('.cfg tbody tr').live('click', function (e) {			// jQuery 1.3+
	//Array.prototype.forEach.call(qa(".cfg tbody tr"), function(el) {
	//	el.onclick = function(e) {
	//http://karmacdesign.webcindario.com/2013/06/27/simular-la-funcion-live-de-jquery-con-javascript.html
	document.onclick = function(event) {
		// Guardamos el elemento sobre el que se ha pinchado.
		var event = event || window.event; // IE compatibility
		var el = event.target || event.srcElement;

		if (el.className != "edit" || el.nodeName != "B") {
			return true;
		}
	
		Array.prototype.forEach.call(qa(".cfg tbody tr"), function(row) {
			removeClass(row,'row_selected');
		});
		
		addClass(el.parentNode.parentNode.parentNode,'row_selected');
	
		var tr = el.parentNode.parentNode.parentNode.cloneNode(true);//tr
		
		if (el.parentNode.parentNode.parentNode.parentNode.parentNode.id == "control_table"){
			tr.children[2].id = "value";
			//tr.children[4].id = "minmax";

			tr.children[2].firstChild.value.removeAttribute('readonly');
			tr.children[2].firstChild.value.removeAttribute('disabled');
				
			//tr.deleteCell(5);
			tr.deleteCell(3);
			tr.deleteCell(1);
			//
			tr.insertCell(2);
			tr.insertCell(3);
			tr.children[3].id = "minmax";
				
			q('#modal_settings .modal-header h3').innerHTML = 'Editar orden';
			updateTips(q('#Tips'),'<p><b>Tenga cuidado!</b><br>Se va a ejecutar una orden sobre del equipo.</p>');
		}
		else{
			tr.deleteCell(4);
			
			if (tr.querySelector('input[name=value]')){
				if (isValidDate(tr.querySelector('input[name=value]').value)){
					tr.children[1].id = "fecha";
				}
				else{
					tr.children[1].id = "value";
				}
			}
			else{
				tr.children[1].id = "value";
			}
			
			tr.children[3].id = "minmax";
		
			Array.prototype.forEach.call(tr.children.item(1).querySelectorAll("input[type=checkbox],input[type=text],select"), function(el) {
				el.removeAttribute('disabled');
				el.removeAttribute('readonly');
			});
		
			q('#modal_settings .modal-header h3').innerHTML = 'Editar parámetro';
			updateTips(q('#Tips'),'<p><b>Tenga cuidado!</b><br>Se va a modificar un parámetro de funcionamiento del equipo.</p>');
		}
		
		//Borrar anterior
		removeRows('ditable');
			
		q('#ditable tbody').appendChild(tr);
			
		d('modal_settings').style.display = '';
		
		Array.prototype.forEach.call(qa('#ditable input[type=text]'), function(el) {
			el.onkeypress = function(e) { 
				var e = e || window.event; // IE compatibility
				if (e.keyCode == 13) {
					if (e.preventDefault) { e.preventDefault(); } else { e.returnValue = false; }
					var myeve = document.createEvent("HTMLEvents");
					myeve.initEvent("click", true, true);					
					d('OK_modal_settings').dispatchEvent(myeve);
				}
			};
		});
	};
	
	Array.prototype.forEach.call(qa('[data-dismiss="modal"]'), function(el) {
		el.onclick = function(e) {
			this.parentNode.parentNode.style.display = 'none';
			//return false;
		};
	});
	
	d('OK_modal_settings').onclick = function() { 
		var bValid = true;
		var fecha = q('#fecha');
		var minmax = q('#minmax');
		
		//if (fecha.length){
		if(fecha != null){
			//fecha.removeClass( "alert-error" );
			//var s = fecha.find('input[type=text]').val();
			
			removeClass(fecha,'alert-error');
			var s = q('#fecha input[type=text]').value;
			
			bValid = bValid && isValidDate(s);
			if ( bValid ) {
				tinyxhr('setTime.cgi?t=' + s,
					function (err,data,xhr){ 
						if (err){
							console.log("goterr ",err,'status='+xhr.status);
						}
					},
					"GET",
					null,
					"html",
					TIMEOUT	// in milliseconds
				);	
				
				d('modal_settings').style.display = 'none';
				//addClass(d('modal_settings'), 'hide');
													
				var ele = q('#modal_settings form input[name=ielem]').value;
				var ind = q('#modal_settings form input[name=index]').value;
				
				if (ele == 6){		//Alarmas
					//qa('#alarms_config li a').item(ind).click();
					var myeve = document.createEvent("HTMLEvents");
					myeve.initEvent("click", true, true);					
					qa('#alarms_config li a').item(ind).dispatchEvent(myeve);
				}
				else if (ele == 5){	//Ordenes
					control_ajax.restart();
				}
				else{				//Settings
					//qa('.settings_tree li a').item(ele).click();
					var myeve = document.createEvent("HTMLEvents");
					myeve.initEvent("click", true, true);					
					qa('.settings_tree li a').item(ele).dispatchEvent(myeve);
				}
			}
			else{
				addClass(fecha, "alert-error");
				updateTips(q('#Tips'), '<p><b>VALOR INCORRECTO:</b> ' + q('#fecha input[type=text]').value + '<br>Formato (hh:mm DD/MM/YY).</p>' );
			}
		}
		else{
											
			var value = q('#value');
												
			removeClass(value,'alert-error');
												
			var min = parseFloat(minmax.textContent.substring(1));
			//var im = minmax.textContent.indexOf("..") + 2;
			var im = minmax.textContent.lastIndexOf("/") + 1;
			var max = parseFloat(minmax.textContent.substring(im));
			
			var ele = q('#modal_settings form input[name=ielem]').value;
			var ind = q('#modal_settings form input[name=index]').value;
			var ivar = q('#modal_settings form input[name=ivar]').value;
			
			var valor;
									
			if (!isNaN(min) && !isNaN(max)){
				valor = q('#value input[type=text]').value;
				bValid = bValid && checkValue(valor,min, max );
			}
			else if( (ele == 6) && ((ivar == 5)||(ivar == 6))){//Caso especial si ELEM_EVE y parametro RELES_W
				var cont = qa('#modal_settings form input[type=checkbox]').length;
				
				var valRele = 0, numCheck = 0, aux = 1;
				valor = 0;
				for( var s = 0; s < cont; s++ ){
					if( qa('#modal_settings form input[type=checkbox]')[s].checked ){
						numCheck = numCheck + 1;
						valRele = qa('#modal_settings form input[type=checkbox]')[s].value;
						aux = 1;
						aux = aux << valRele;
						valor = valor | aux;
					}
				}
			}
			else{
				valor = q('#modal_settings form select').selectedIndex;
			}
											
			if ( bValid ) {
				var ref = "ielem=" + ele + "&index=" + ind + "&ivar=" + ivar;

				tinyxhr('setParam.cgi?' + ref + '&value=' + valor,
					function (err,data,xhr){ 
						if (err){
							console.log("goterr ",err,'status='+xhr.status);
						}
					},
					"GET",
					null,
					"html",
					TIMEOUT	// in milliseconds
				);							
													
				d('modal_settings').style.display = 'none';
				
				if (ele == 6){		//Alarmas
					//qa('#alarms_config li a').item(ind).click();
					var myeve = document.createEvent("HTMLEvents");
					myeve.initEvent("click", true, true);					
					qa('#alarms_config li a').item(ind).dispatchEvent(myeve);
				}
				else if (ele == 5){	//Ordenes
					control_ajax.restart();
				}
				else{				//Settings
					//qa('.settings_tree li a').item(ele).click();
					var myeve = document.createEvent("HTMLEvents");
					myeve.initEvent("click", true, true);					
					qa('.settings_tree li a').item(ele).dispatchEvent(myeve);
				}
				//aibabe
			}
			else{
				addClass(value, "alert-error");
				updateTips(q('#Tips'), '<p><b>VALOR INCORRECTO:</b> ' + q('#value input[type=text]').value + '<br>El valor debe estar en el intervalo' + ' (' + min + ' - ' + max + ').</p>' );
			}
		}
	};
});

/*<!-- Alarms-->*/
function updateAlarms(htmlData) {

	if(!htmlData)
	{
		return;
	}

	//var sp = htmlData.split(/<\s*tr[^>]*>(.*?)<\s*\/\s*tr>/g);
	var sp = htmlData.split(/(?:<tr>|<\/tr>)/ig);
	var filas = [];
	for (var i=0;i<sp.length;i++){
		//if ((i+2)%2==1) {
		if (sp[i] != '') {
			filas.push(sp[i]);
		}
	}
	
	//for(var i = 0; i < (jsonData.aaData.length); i++) {
	for(var j = 0; j < (filas.length); j++) {
		//sp = filas[j].split(/<\s*td[^>]*>(.*?)<\s*\/\s*td>/g);
		sp = filas[j].split(/(?:<td>|<\/td>)/ig);
		var fila = [];
		for (var i=0;i<sp.length;i++){
			if ((i%2)==1) {			
			//if (sp[i] != '') {
				fila.push(sp[i]);
			}
		}
		
		if (ielem==6){
			addRow('alarms_table',fila);
		}
		else{
			addRow('alarms_his_table',fila);
		}
	}
	
	flushThis('#alarms_his_tab');
	
	var total = na;
	
	if (ielem == 7){
		total = nh;
	}				
	
	index = index + filas.length;
	if (index > total){
		index = total;
	}
	var percentVal = Math.floor((index)/total*100) + '%';
	d('alarms_his_bar').style.width = percentVal;
	d('alarms_his_bar').parentNode.style.display = '';
	d('alarms_his_bar').innerHTML = percentVal;
	
	if (index < total){
		//index = index + jsonData.aaData.length;
		//index = index + filas.length;
		alarms_ajax.restart();
	}
	else{
		if (ielem == 7){
			setTimeout(function() {
				d('alarms_his_bar').parentNode.style.display = 'none';
			}, 3000 );
		}
		alarms_ajax.cancel();
	}
}

function updateAlarmsSettings(htmlData) {
	if(!htmlData)
	{
		return;
	}

	removeRows('alarms_cfg_table');

	var jsonData = eval( "(" + htmlData + ")" );
	
	for(var i = 0; i < (jsonData.aaData.length); i++) {
	
		var fila = new Array();
		fila = jsonData.aaData[i];
		
		var iElem = parseInt(jsonData.aaData[i][1][0]);
		var indice = parseInt(jsonData.aaData[i][1][1]);
		var ivar = parseInt(jsonData.aaData[i][1][2]);
		var valor = jsonData.aaData[i][1][3];
		
		var select;
		if (jsonData.aaData[i][1][4]){
			select = "<select name='value' disabled>"; 
			for(var j = 0; j < (jsonData.aaData[i][1][4].length); j++) {
				if (jsonData.aaData[i][1][4][j] == valor){
					select = select + "<option value='" + j + "' selected='true'>" + jsonData.aaData[i][1][4][j] + "</option>";
				}
				else{
					select = select + "<option value='" + j + "'>" + jsonData.aaData[i][1][4][j] + "</option>";
				}
			}	
			select = select + "</select>";
		}
		else{
			select = "<input readonly type='text' name='value' value='" + valor + "' />";
		}

		if( (iElem == 6) && ((ivar == 5)||(ivar == 6))){//Caso especial si ELEM_EVE y parametro RELES_W
			var codHtml;
			codHtml = "";
			
			if (ivar == 5){
				for ( var s = 0; s < 4; s++ ){
					var aux = 1;
					aux = aux << s;
					codHtml = codHtml + "<input disabled type='checkbox' name='R" + (s + 1).toString() + "' ";
					codHtml = codHtml + "value='" + s.toString() + "'";
					if(valor & aux){//El bit 0 indica Rele 1, el bit 1 indica Rele 2, etc
						codHtml = codHtml + " checked";
					}
					codHtml = codHtml + " >" + " Relé " + (s + 1).toString() + "<br>";
				}
			}
			else  if(ivar == 6){
				for ( var s = 0; s < 3; s++ ){
					var aux = 1;
					aux = aux << s;
					codHtml = codHtml + "<input disabled type='checkbox' name='L" + (s + 1).toString() + "' ";
					codHtml = codHtml + "value='" + s.toString() + "'";
					if(valor & aux){//El bit 0 indica Rele 1, el bit 1 indica Rele 2, etc
						codHtml = codHtml + " checked";
					}
					codHtml = codHtml + " >" + " Led " + (s + 1).toString() + "<br>";
				}
			}

			select = codHtml;
			fila[3] = "";
		}
		
		fila[1] = "<form id='form-simple'>" +
						"<input type='hidden' name='ielem' value='" + iElem + "'/>" +
						"<input type='hidden' name='index' value='" + indice + "'/>" +
						"<input type='hidden' name='ivar' value='" + ivar + "'/>" +
						"<span>" + select + "</span>" +
					"</form>";
		
		if (parseInt(jsonData.aaData[i][4])){
			//fila[4] = "<a href='javascript:void(0)' class='icn-pencil icn-2x'><span style='display:none'>1</span></a>";
			fila[4] = "<a href='javascript:void(0)'><b style='font-size: 20px' class='edit'>&#x270e;*</b><span style='display:none'>1</span></a>";
			
		}
		else{
			fila[4] = "<span style='display:none'>0</span>";
		}
		
		addRow('alarms_cfg_table',fila);
	}
}

//$(document).ready(function() {
addListener(window, 'load', function() {

	alarms_ajax = periodic({period: PERIODO, decay: 1, max_period: 60000}, function() {
		if(xhr_alarms && xhr_alarms.readystate != 4){
			xhr_alarms.abort();
		}
		xhr_alarms = tinyxhr('alarms.cgi?ielem=' + ielem + '&index=' + index,
			function (err,data,xhr){
			
				alarms_ajax.cancel();
				
				if (err){
					console.log("Alarms Failed");
					console.log("goterr ",err,'status='+xhr.status);
					alarms_ajax.restart();
				}
				else{
					console.log("Alarms Success");
					updateAlarms(data);
				}
				//alarms_ajax.cancel();
			},
			"GET",
			null,
			"html",
			TIMEOUT	// in milliseconds
		);
	});
	
	d('btn_demoModal1').onclick = function() { 
		d('demoModal1').style.display = '';
	};
	
	d('btn_demoModal2').onclick = function() { 
		//if (hasClass(this,'pure-button-disabled')) { return false; }
		d('demoModal2').style.display = '';
	};
	
	d('btn_demoModal3').onclick = function() { 
		d('demoModal3').style.display = '';
	};
	
	//$('#btn_recognize').click( function() {
	d('btn_recognize').onclick = function() {
		tinyxhr('orden.cgi?ivar=3&value=0',
			function (err,data,xhr){ 
			
				d('demoModal1').style.display = 'none';
				
				if (err){
					console.log("goterr ",err,'status='+xhr.status);
				}
				else{
					//$('#modalalert1').modal('show');
					d('modalalert1').style.display = '';
					//$('#alarms .nav-secondary a:first').trigger('click');
					//q('#alarms_id .navs a[href="#alarms_act_tab"]').click();
					var myeve = document.createEvent("HTMLEvents");
					myeve.initEvent("click", true, true);					
					q('#alarms_id .navs a[href="#alarms_act_tab"]').dispatchEvent(myeve);
				}
			},
			"GET",
			null,
			"html",
			TIMEOUT	// in milliseconds
		);
	//});
	};
	
	d('btn_remove').onclick = function() {
		tinyxhr('orden.cgi?ivar=3&value=1',
			function (err,data,xhr){ 
			
				d('demoModal2').style.display = 'none';
				
				if (err){
					console.log("goterr ",err,'status='+xhr.status);
				}
				else{
					d('modalalert2').style.display = '';
					//$('#alarms .nav-secondary a[href="#alarms_his"]').trigger('click');
					//q('#alarms_id .navs a[href="#alarms_his_tab"]').click();
					var myeve = document.createEvent("HTMLEvents");
					myeve.initEvent("click", true, true);
					q('#alarms_id .navs a[href="#alarms_his_tab"]').dispatchEvent(myeve);
				}
			},
			"GET",
			null,
			"html",
			TIMEOUT	// in milliseconds
		);
	};
	
	d('btn_cfghisremove').onclick = function() {
		tinyxhr('orden.cgi?ivar=3&value=2',
			function (err,data,xhr){ 
			
				d('demoModal3').style.display = 'none';
				
				if (err){
					console.log("goterr ",err,'status='+xhr.status);
				}
				else{
					d('modalalert3').style.display = '';
					//$('#settings_his .nav-secondary a[href="#settings_his"]').trigger('click');
					//q('#settings_id .navs a[href="#settings_his_tab"]').click();
					var myeve = document.createEvent("HTMLEvents");
					myeve.initEvent("click", true, true);					
					q('#settings_id .navs a[href="#settings_his_tab"]').dispatchEvent(myeve);
				}
			},
			"GET",
			null,
			"html",
			TIMEOUT	// in milliseconds
		);
	};
	
	alarm_settings_ajax = periodic({period: PERIODO, decay: 1, max_period: 60000}, function() {

		if(xhr_alarm_settings && xhr_alarm_settings.readystate != 4){
			xhr_alarm_settings.abort();
		}
		
		//d('alarm_settings_load').getElementsByTagName('small')[0].innerHTML = 'Cargando...';
		
		//d('status_load').innerHTML = 'Cargando...';
		showAjax();
		
		xhr_alarm_settings = tinyxhr('settings.cgi?ielem=' + ielem + '&index=' + index,
			function (err,data,xhr){ 
				if (err){
					console.log("Alarm Settings Failed");
					console.log("goterr ",err,'status='+xhr.status);
					//d('alarm_settings_load').getElementsByTagName('small')[0].innerHTML = "<span class='uni'>&#9888;</span> Fallo COM";
					//d('status_load').innerHTML = "<span class='uni'>&#9888;</span> Fallo COM";
					showError();
				}
				else{
					console.log("Alarm Settings Success");
					//d('alarm_settings_load').getElementsByTagName('small')[0].innerHTML = " ";
					//d('status_load').innerHTML = " ";
					hideAjax();
					updateAlarmsSettings(data);
					d('alarms_cfg_table').tBodies[0].style.display = '';
				}

				alarm_settings_ajax.cancel();
				hideLoader();
			},
			"GET",
			null,
			"html",
			TIMEOUT	// in milliseconds
		);
	});

	Array.prototype.forEach.call(qa("#alarms_config li a"), function(el) {
		el.onclick = function(e) {
		
			var e = e || window.event; // IE compatibility
			(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
		
			var tli = this;
			Array.prototype.forEach.call(qa("#alarms_config li a"), function(a,i) {
				if (qa("#alarms_config li a").item(i) == tli){
					index = i;
				}
			});
			
			//$('.settings_tree .nav li.active').removeClass('active');
			removeClass(q('#alarms_config li.selected'),'selected');
		
			//$(this).closest("li").addClass('active');
			//this.parentNode.className += 'selected';
			
			addClass(this.parentNode, 'selected');
			
			ielem = 6;
			
			//originalHash = this.hash.split('_li')[0];
			//location.hash = this.hash.split('_li')[0];
			//d('aBar').innerHTML = this.lastChild.nodeValue;
			
			qa('.breadcrumb span')[3].textContent = " › " + this.lastChild.nodeValue;
			//qa('.breadcrumb span')[3].innerHTML = " / " + this.innerHTML;
			
			alarm_settings_ajax.restart();
			
			showLoader(d('alarms_cfg_table'));
			
			return false;
		};
	});
		
});

/*<!-- Control-->*/
function updateControl(htmlData) {

	if(!htmlData)
	{
		return;
	}

	removeRows('control_table');
	var jsonData= eval( "(" + htmlData + ")" );
	
	for(var i = 0; i < (jsonData.aaData.length); i++) {
		var fila = new Array();
		fila = jsonData.aaData[i];
		var iElem = parseInt(jsonData.aaData[i][2][0]);
		var indice = parseInt(jsonData.aaData[i][2][1]);
		var ivar = parseInt(jsonData.aaData[i][2][2]);
		var valor = jsonData.aaData[i][2][3];
		
		var select;
		if (jsonData.aaData[i][2][4]){
			select = "<select name='value' disabled>"; 
			for(var j = 0; j < (jsonData.aaData[i][2][4].length); j++) {
				if (jsonData.aaData[i][2][4][j] == valor){
					select = select + "<option value='" + j + "' selected='true'>" + jsonData.aaData[i][2][4][j] + "</option>";
				}
				else{
					select = select + "<option value='" + j + "'>" + jsonData.aaData[i][2][4][j] + "</option>";
				}
			}	
			select = select + "</select>";
		}
		else{
			select = "<input readonly type='text' name='value' value='" + valor + "' />";
		}

		fila[2] = "<form id='form-simple'>" +
						"<input type='hidden' name='ielem' value='" + iElem + "'/>" +
						"<input type='hidden' name='index' value='" + indice + "'/>" +
						"<input type='hidden' name='ivar' value='" + ivar + "'/>" +
						"<span>" + select + "</span>" +
					"</form>";
		
		//if (parseInt(jsonData.aaData[i][5])){
		if (parseInt(jsonData.aaData[i][3])){
			//fila[5] = "<a href='javascript:void(0)' class='icn-pencil icn-2x'><span style='display:none'>1</span></a>";
			//fila[5] = "<a href='javascript:void(0)'><b style='font-size: 20px' class='edit'>&#x270e;*</b><span style='display:none'>1</span></a>";
			fila[3] = "<a href='javascript:void(0)'><b style='font-size: 20px' class='edit'>&#x270e;*</b><span style='display:none'>1</span></a>";
		}
		else{
			//fila[5] = "<span style='display:none'>0</span>";
			fila[3] = "<span style='display:none'>0</span>";
		}
		
		addRow('control_table',fila);
	}
	
	ielem_ant = ielem;
	index_ant = index;
}

//$(document).ready(function() {
addListener(window, 'load', function() {

	control_ajax = periodic({period: PERIODO, decay: 1, max_period: 60000}, function() {
		if(xhr_control && xhr_control.readystate != 4){
			xhr_control.abort();
		}
		
		//d('control_load').getElementsByTagName('small')[0].innerHTML = 'Cargando...';
		//d('status_load').innerHTML = 'Cargando...';
		showAjax();
		
		xhr_control = tinyxhr('settings.cgi?ielem=' + ielem + '&index=' + index,
			function (err,data,xhr){ 
				if (err){
					console.log("goterr ",err,'status='+xhr.status);
					//d('control_load').getElementsByTagName('small')[0].innerHTML = "<span class='uni'>&#9888;</span> Fallo COM";
					//d('status_load').innerHTML = "<span class='uni'>&#9888;</span> Fallo COM";
					showError();
				}
				else{
					if (!d("AjaxCheckbox").checked){
						//d('control_load').getElementsByTagName('small')[0].innerHTML = " ";
						//d('status_load').innerHTML = " ";
						hideAjax();
					}
					updateControl(data);
					d('control_table').tBodies[0].style.display = '';
				}

				if (!d("AjaxCheckbox").checked){
					//this.periodic.cancel();
					control_ajax.cancel();
				
					//d('status_load').getElementsByTagName('small')[0].innerHTML = " ";
					//fadeOut(750,d('status_load'));
					//load_st.cancel();
				}
				
				//$.hideLoader();
				hideLoader();
				//d('settings_table').tBodies[0].style.display = '';
			},
			"GET",
			null,
			"html",
			TIMEOUT	// in milliseconds
		);
	});
});

/*<!-- Network-->*/

function configIPBoxes(obj) {
	if (!obj.disabled){
		q("#rip").disabled = obj.checked;
		q("#gw").disabled = obj.checked;
		q("#rsub").disabled = obj.checked;
		//q("#dns1").disabled = obj.checked;
		//q("#dns2").disabled = obj.checked;
		q("#dns1").disabled = true;
		q("#dns2").disabled = true;		
	}
}

function configNTPBoxes(obj) {
	if (!obj.disabled){
		q('input[name="ntpp"]').disabled = !obj.checked;
		//q('input[name="ntps"]').disabled = !obj.checked;
		q('input[name="tpol"]').disabled = !obj.checked;
		q('input[name="tesp"]').disabled = !obj.checked;
		q('input[name="ntry"]').disabled = !obj.checked;
		q('input[name="ttry"]').disabled = !obj.checked;
		q('input[name="terr"]').disabled = !obj.checked;
	}
}

function configLDAPBoxes(obj) {
	//q('select[name="regi"]').disabled = !obj.checked;
	if (!obj.disabled){
		q('input[name="hdap"]').disabled = !obj.checked;
		q('input[name="pdap"]').disabled = !obj.checked;
		q('input[name="bdap"]').disabled = !obj.checked;
		q('input[name="sdap"]').disabled = !obj.checked;
		q('textarea[name="fdap"]').disabled = !obj.checked;
		//q('input[name="adap"]').disabled = !obj.checked;
	}
}

function configSNMPBoxes(obj) {
	if (!obj.disabled){
		q('input[name="trcv"]').disabled = !obj.checked;
		q('input[name="ttim"]').disabled = !obj.checked;
		q('input[name="tcm0"]').disabled = !obj.checked;
	}
}

function updateEquipo(xmlData) {
	if(!xmlData)
	{
		return;
	}
	
	//EQUIPO
	q('input[name="tipo"]').value = getAXMLValue(xmlData,'EQUIPO','tipo');
	q('input[name="mod"]').value = getAXMLValue(xmlData,'EQUIPO','modelo');
	q('input[name="fab"]').value = getAXMLValue(xmlData,'EQUIPO','fabricante');
	q('input[name="nser"]').value = getAXMLValue(xmlData,'EQUIPO','num_serie');
	q('input[name="cod"]').value = getAXMLValue(xmlData,'EQUIPO','codigo');
	q('input[name="hard"]').value = getAXMLValue(xmlData,'REV_HARD','hard');	
	q('input[name="soft"]').value = getAXMLValue(xmlData,'REV_FW_SW','ver_sw');
	q('input[name="iden"]').value = getAXMLValue(xmlData,'REV_FW_SW','ident_soft');
	q('input[name="ver"]').value = getAXMLValue(xmlData,'REV_CONF','ver_conf');
	q('input[name="date"]').value = getAXMLValue(xmlData,'REV_CONF','date_conf');
	q('input[name="bat"]').value = getAXMLValue(xmlData,'TIPO','tec');
	q('input[name="loc"]').value = getAXMLValue(xmlData,'NOM_INSTALAC','instalacion');
	q('input[name="sig"]').value = getAXMLValue(xmlData,'COD_SIGRID','sigrid');
	
	
	q('input[name="usl"]').value = getAXMLValue(xmlData,'LOCAL_USER','usuario');
	q('input[name="usn"]').value = getAXMLValue(xmlData,'LOCAL_USER','usuario');	
	q('input[name="pul"]').value = getAXMLValue(xmlData,'LOCAL_USER','password');
	q('input[name="adl"]').value = getAXMLValue(xmlData,'LOCAL_ADMIN','usuario');
	q('input[name="adn"]').value = getAXMLValue(xmlData,'LOCAL_ADMIN','usuario');
	q('input[name="pal"]').value = getAXMLValue(xmlData,'LOCAL_ADMIN','password');
}

function updateNetwork(xmlData) {
	if(!xmlData)
	{
		return;
	}
	
	//IP
	
	//q('input[name="loc"]').value = getXMLValue(xmlData,'loc');
	//q('input[name="mac"]').value = getXMLValue(xmlData,'MAC');
	//q('input[name="host"]').value = getXMLValue(xmlData,'host');
	
	q('input[name="mac"]').value = getAXMLValue(xmlData,'LOCAL','mac');

	//if (parseInt( getXMLValue(xmlData,'DHCP') ) == 1 ){
	if (parseInt( getAXMLValue(xmlData,'IP_DINAMICA','dhcp') ) == 1 ){
		q('input[name="dhcp"]').checked = true;
	}
	else{
		q('input[name="dhcp"]').checked = false;
	}
	
	configIPBoxes(q("input[name=dhcp]"));

	/*
	q('input[name="rip"]').value = getXMLValue(xmlData,'R_IP');
	q('input[name="gw"]').value = getXMLValue(xmlData,'R_GTW');
	q('input[name="rsub"]').value = getXMLValue(xmlData,'R_MASK');
	q('input[name="dns1"]').value = getXMLValue(xmlData,'R_DNS0');
	q('input[name="dns2"]').value = getXMLValue(xmlData,'R_DNS1');
	q('input[name="dip"]').value = getXMLValue(xmlData,'L_IP');
	q('input[name="dsub"]').value = getXMLValue(xmlData,'L_MASK');
	*/
	
	q('input[name="rip"]').value = getAXMLValue(xmlData,'REMOTO','remote_ip');
	q('input[name="gw"]').value = getAXMLValue(xmlData,'REMOTO','remote_gtw');
	q('input[name="rsub"]').value = getAXMLValue(xmlData,'REMOTO','remote_mask');
	q('input[name="dns1"]').value = getAXMLValue(xmlData,'REMOTO','remote_dns0');
	q('input[name="dns2"]').value = getAXMLValue(xmlData,'REMOTO','remote_dns1');
	q('input[name="dip"]').value = getAXMLValue(xmlData,'LOCAL','ip');
	q('input[name="dsub"]').value = getAXMLValue(xmlData,'LOCAL','mask');
	
	//NTP
	//if (parseInt( getXMLValue(xmlData,'NTP_HAB') ) == 1 ){
	if (parseInt( getAXMLValue(xmlData,'NTP','ntp') ) == 1 ){
		q('input[name="ntp"]').checked = true;
	}
	else{
		q('input[name="ntp"]').checked = false;
	}
	
	configNTPBoxes(q("input[name=ntp]"));
	
	/*
	q('input[name="ntpp"]').value = getXMLValue(xmlData,'NTP_IP_PRIN');
	q('input[name="ntps"]').value = getXMLValue(xmlData,'NTP_IP_SEC');
	q('input[name="tpol"]').value = getXMLValue(xmlData,'NTP_TPOLL');
	q('input[name="tesp"]').value = getXMLValue(xmlData,'NTP_TESP');
	q('input[name="ntry"]').value = getXMLValue(xmlData,'NTP_NTRY');
	q('input[name="terr"]').value = getXMLValue(xmlData,'NTP_TFAIL');
	*/
	q('input[name="ntpp"]').value = getAXMLValue(xmlData,'NTP','ip_prin');
	//q('input[name="ntps"]').value = getAXMLValue(xmlData,'NTP','ip_sec');
	q('input[name="tpol"]').value = getAXMLValue(xmlData,'NTP','tpoll');
	q('input[name="tesp"]').value = getAXMLValue(xmlData,'NTP','tesp');
	q('input[name="ntry"]').value = getAXMLValue(xmlData,'NTP','ntry');
	q('input[name="ttry"]').value = getAXMLValue(xmlData,'NTP','ttry');
	q('input[name="terr"]').value = getAXMLValue(xmlData,'NTP','tfail');
	
	//LDAP
	//if (parseInt( getXMLValue(xmlData,'LDAP_HAB') ) == 1 ){
	if (parseInt( getAXMLValue(xmlData,'LDAP','ldap') ) == 1 ){
		q('input[name="ldap"]').checked = true;
	}
	else{
		q('input[name="ldap"]').checked = false;
	}
	configLDAPBoxes(q("input[name=ldap]"));
	/*
	q('input[name="cat1"]').value = getXMLValue(xmlData,'CAT1');
	q('input[name="cat2"]').value = getXMLValue(xmlData,'CAT2');
	q('input[name="cat3"]').value = getXMLValue(xmlData,'CAT3');
	q('input[name="cat4"]').value = getXMLValue(xmlData,'CAT4');
	q('input[name="cat5"]').value = getXMLValue(xmlData,'CAT5');
	q('input[name="cat6"]').value = getXMLValue(xmlData,'CAT6');
	q('input[name="cat7"]').value = getXMLValue(xmlData,'CAT7');
	q('input[name="cat8"]').value = getXMLValue(xmlData,'CAT8');
	q('input[name="hdap"]').value = getXMLValue(xmlData,'LDAP_IP');
	q('input[name="pdap"]').value = getXMLValue(xmlData,'LDAP_PORT');
	q('input[name="bdap"]').value = getXMLValue(xmlData,'LDAP_BIND_DN');
	q('input[name="sdap"]').value = getXMLValue(xmlData,'LDAP_SEARCH_DN');
	q('input[name="fdap"]').value = getXMLValue(xmlData,'LDAP_FILTER');
	*/
	
	q('input[name="tact"]').value = getAXMLValue(xmlData,'LDAP','tact');	
	q('input[name="cat1"]').value = getAXMLValue(xmlData,'LDAP','cat1');
	q('input[name="cat2"]').value = getAXMLValue(xmlData,'LDAP','cat2');
	q('input[name="cat3"]').value = getAXMLValue(xmlData,'LDAP','cat3');
	q('input[name="cat4"]').value = getAXMLValue(xmlData,'LDAP','cat4');
	q('input[name="cat5"]').value = getAXMLValue(xmlData,'LDAP','cat5');
	q('input[name="cat6"]').value = getAXMLValue(xmlData,'LDAP','cat6');
	q('input[name="cat7"]').value = getAXMLValue(xmlData,'LDAP','cat7');
	q('input[name="cat8"]').value = getAXMLValue(xmlData,'LDAP','cat8');
	q('input[name="hdap"]').value = getAXMLValue(xmlData,'LDAP','ldap_ip');
	q('input[name="pdap"]').value = getAXMLValue(xmlData,'LDAP','ldap_port');
	q('input[name="bdap"]').value = getAXMLValue(xmlData,'LDAP','bind');
	q('input[name="sdap"]').value = getAXMLValue(xmlData,'LDAP','search');
	q('textarea[name="fdap"]').value = getAXMLValue(xmlData,'LDAP','filter');	
	
	//q('input[name="adap"]').value = getXMLValue(xmlData,'adap');
	
	//SNMP
	//if (parseInt( getXMLValue(xmlData,'TRAPS_HAB') ) == 1 ){
	if (parseInt( getAXMLValue(xmlData,'SNMP','traps') ) == 1 ){
		q('input[name="snmp"]').checked = true;
	}
	else{
		q('input[name="snmp"]').checked = false;
	}
	
	configSNMPBoxes(q("input[name=snmp]"));
	/*
	q('input[name="rcm0"]').value = getXMLValue(xmlData,'READ_COM');
	q('input[name="wcm0"]').value = getXMLValue(xmlData,'WRITE_COM');
	q('input[name="trcv"]').value = getXMLValue(xmlData,'TRAPS_IP');
	q('input[name="ttim"]').value = getXMLValue(xmlData,'TRAPS_T');
	q('input[name="tcm0"]').value = getXMLValue(xmlData,'TRAPS_COM');
	*/
	
	q('input[name="rcm0"]').value = getAXMLValue(xmlData,'SNMP','rcom');
	q('input[name="wcm0"]').value = getAXMLValue(xmlData,'SNMP','wcom');
	q('input[name="trcv"]').value = getAXMLValue(xmlData,'SNMP','trap_ip');
	q('input[name="ttim"]').value = getAXMLValue(xmlData,'SNMP','trap_period');
	q('input[name="tcm0"]').value = getAXMLValue(xmlData,'SNMP','trap_com');
	
	//EQUIPO
	/*
	q('input[name="tipo"]').value = getXMLValue(xmlData,'TIPO');
	q('input[name="mod"]').value = getXMLValue(xmlData,'MODELO');
	q('input[name="fab"]').value = getXMLValue(xmlData,'FABRICANTE');
	q('input[name="nser"]').value = getXMLValue(xmlData,'N_SERIE');
	q('input[name="soft"]').value = getXMLValue(xmlData,'REVISION');
	q('input[name="loc"]').value = getXMLValue(xmlData,'INSTALACION');
	q('input[name="sig"]').value = getXMLValue(xmlData,'SIGRID');
	*/
	/*
	q('input[name="tipo"]').value = getAXMLValue(xmlData,'EQUIPO','tipo');
	q('input[name="mod"]').value = getAXMLValue(xmlData,'EQUIPO','modelo');
	q('input[name="fab"]').value = getAXMLValue(xmlData,'EQUIPO','fabricante');
	q('input[name="nser"]').value = getAXMLValue(xmlData,'EQUIPO','num_serie');
	q('input[name="cod"]').value = getAXMLValue(xmlData,'EQUIPO','codigo');
	q('input[name="hard"]').value = getAXMLValue(xmlData,'REV_HARD','hard');	
	q('input[name="soft"]').value = getAXMLValue(xmlData,'REV_FW_SW','soft');
	q('input[name="ver"]').value = getAXMLValue(xmlData,'REV_FW_SW','version');
	q('input[name="iden"]').value = getAXMLValue(xmlData,'REV_FW_SW','ident_soft');
	q('input[name="date"]').value = getAXMLValue(xmlData,'REV_FW_SW','date');
	q('input[name="bat"]').value = getAXMLValue(xmlData,'TIPO','tec');
	q('input[name="loc"]').value = getAXMLValue(xmlData,'NOM_INSTALAC','instalacion');
	q('input[name="sig"]').value = getAXMLValue(xmlData,'COD_SIGRID','sigrid');
	*/

}


var count;
var redirect = "http://192.168.33.100"; 

function countDown(cuenta){  
	if (cuenta){
		count = cuenta;
	}
	if (count <=0){  
		window.location = redirect;  
	}else{  
		count--;  
		
		d('timer').innerHTML = " Esta página se redirigirá en <b>"+count+"</b> segundos.";
		setTimeout(countDown, 1000)  
	}  
}

//$("#lntp").click(function() {  //function(event) {
d('lntp').onclick = function() {
	//event.preventDefault();
	
	changeBtnColor(this, 'pure-button-disabled' ,2500);
	tinyxhr('orden.cgi?ivar=6&value=2',
		function (err,data,xhr){ 
			if (err){
				console.log("goterr ",err,'status='+xhr.status);
				console.log("Leer NTP Defecto Failed!");
			}
			else{
				console.log("Leer NTP Defecto Success");
				network_ajax.restart();
			}
		},
		"GET",
		null,
		"html",
		TIMEOUT	// in milliseconds
	);
};

function checkForm(username,pwd1,pwd2,tip) {
	if(username.value == "") {
		//alert("Error: El Username no puede estar en blanco!");
		updateTips(tip,'<p><b>Error:</b><br>El Username no puede estar en blanco!</p>');
		username.focus();
		return false;
	}
	/*
	re = /^\w+$/;
	if(!re.test(username.value)) {
		//alert("Error: El Username debe contener solo letras,números y subrayados!");
		updateTips(tip,'<p><b>Error:</b><br>El Username debe contener solo letras,números y subrayados!</p>');
		username.focus();
		return false;
	}
	*/
	if(pwd1.value != "" && pwd1.value == pwd2.value) {
		/*
		if(pwd1.value.length < 2) {
			//alert("Error: El Password debe contener más de un caracter!");
			updateTips(tip,'<p><b>Error:</b><br>El Password debe contener más de un caracter!</p>');
			pwd1.focus();
			return false;
		}
		if(pwd1.value == username.value) {
			//alert("Error: El Password debe der diferente del Username!");
			updateTips(tip,'<p><b>Error:</b><br>El Password debe der diferente del Username!</p>');
			pwd1.focus();
			return false;
		}
		*/
		//re = /[0-9]/; 
		//if(!re.test(pwd1.value)) { 
		//	alert("Error: password must contain at least one number (0-9)!"); 
		//	pwd1.focus(); return false; 
		//} 
		//re = /[a-z]/; 
		//if(!re.test(pwd1.value)) { 
		//	alert("Error: password must contain at least one lowercase letter (a-z)!"); 
		//	pwd1.focus(); return false; 
		//} 
		//re = /[A-Z]/; 
		//if(!re.test(pwd1.value)) { 
		//	alert("Error: password must contain at least one uppercase letter (A-Z)!"); 
		//	pwd1.focus(); 
		//	return false; 
		//} 
		
		updateTips(tip,'<p>Ha introducido un password correcto!</p>');
		return true;
	}
	else {
		//alert("Error: Compruebe que ha introducido y confirmado su Password!");
		updateTips(tip,'<p><b>Error:</b><br>Compruebe que ha introducido y confirmado su Password!</p>');
		pwd1.focus();
		//setTimeout(function(){pwd1.focus();},0);
		return false;
	}
	//alert("Ha introducido un password correcto: " + pwd1.value);
	//updateTips(tip,'<p>Ha introducido un password correcto!</p>');
	//return true;
}

d('btn_uspe_modal').onclick = function() {
	updateTips(q('#uspe_modal aside'),'<p><b>Tenga cuidado!</b><br>Se van a modificar los datos de acceso para Usuario Local.</p>');
	d('pswucfg').usn.value = d('pswcfg').usl.value;
	d('pswucfg').pun.value = d('pswucfg').punr.value = "";
	d('uspe_modal').style.display = '';
};

d('btn_adpe_modal').onclick = function() { 
	updateTips(q('#adpe_modal aside'),'<p><b>Tenga cuidado!</b><br>Se van a modificar los datos de acceso para Administrador Local.</p>');
	d('pswacfg').adn.value = d('pswcfg').adl.value;
	d('pswacfg').pan.value = d('pswacfg').panr.value = "";
	d('adpe_modal').style.display = '';
};

d('OK_upsw').onclick = function() {
//d('pswucfg').onsubmit = function(e) {

	// stop form from submitting normally
	var e = e || window.event;
	(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
	
	if (checkForm(d('pswucfg').usn,d('pswucfg').pun,d('pswucfg').punr,q('#uspe_modal aside')) != true){
		return;
	}
	
	d('pswucfg').usn.value = d('pswucfg').usn.value.toUpperCase();
	
	var values = serializeForm('pswucfg');

	tinyxhr('setPSW.htm',
		function (err,data,xhr){ 
		
			d('uspe_modal').style.display = 'none';
			
			if (err){
				console.log("PSW Failed ",err,'status='+xhr.status);
			}
			else{
				console.log("PSW Success");
				setTimeout(function() {
					equipo_ajax.restart();
				},1000);
			}
		},
		"POST",
		values,
		null,
		TIMEOUT
	);
};

d('OK_apsw').onclick = function() { 
//d('pswacfg').onsubmit = function(e) {
	// stop form from submitting normally
	var e = e || window.event;
	(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
	
	if (checkForm(d('pswacfg').adn,d('pswacfg').pan,d('pswacfg').panr,q('#adpe_modal aside')) != true){
		return;
	}
	
	d('pswacfg').adn.value = d('pswacfg').adn.value.toUpperCase();
	
	var values = serializeForm('pswacfg');

	tinyxhr('setPSW.htm',
		function (err,data,xhr){ 
		
			d('adpe_modal').style.display = 'none';
			
			if (err){
				console.log("PSW Failed ",err,'status='+xhr.status);
			}
			else{
				console.log("PSW Success");
				setTimeout(function() {
					equipo_ajax.restart();
				},1000);
			}
		},
		"POST",
		values,
		null,
		TIMEOUT
	);
};

/*
d('OK_gpsw').onclick = function() {
	tinyxhr('orden.cgi?ivar=6&value=2',
		function (err,data,xhr){
			d('uspe_modal').style.display = 'none';
			
			if (err){
				console.log("goterr ",err,'status='+xhr.status);
			}
			else{
				network_ajax.restart();
			}
		},
		"GET",
		null,
		"html",
		TIMEOUT	// in milliseconds
	);
};
*/

d('lpsw').onclick = function() {
	//event.preventDefault();
	
	changeBtnColor(this, 'pure-button-disabled' ,2500);
	tinyxhr('orden.cgi?ivar=6&value=6',
		function (err,data,xhr){ 
			if (err){
				console.log("goterr ",err,'status='+xhr.status);
				console.log("Leer PSW Defecto Failed!");
			}
			else{
				console.log("Leer PSW Defecto Success");
				setTimeout(function() {
					equipo_ajax.restart();
				},1000);
			}
		},
		"GET",
		null,
		"html",
		TIMEOUT	// in milliseconds
	);
};

d('lldap').onclick = function() {
	//event.preventDefault();
	
	changeBtnColor(this, 'pure-button-disabled' ,2500);
	tinyxhr('orden.cgi?ivar=6&value=4',
		function (err,data,xhr){ 
			if (err){
				console.log("goterr ",err,'status='+xhr.status);
				console.log("Leer LDAP Defecto Failed!");
			}
			else{
				console.log("Leer LDAP Defecto Success");
				network_ajax.restart();
				//
				events_ajax.restart();
				//
			}
		},
		"GET",
		null,
		"html",
		TIMEOUT	// in milliseconds
	);
};

d('lloc').onclick = function() {
	//var e = e || window.event; // IE compatibility
	//(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
	
	changeBtnColor(this, 'pure-button-disabled' ,2500);
	tinyxhr('orden.cgi?ivar=6&value=5',
		function (err,data,xhr){ 
			if (err){
				console.log("goterr ",err,'status='+xhr.status);
				console.log("Leer LOC Defecto Failed!");
			}
			else{
				console.log("Leer LOC Defecto Success");
				equipo_ajax.restart();
			}
		},
		"GET",
		null,
		"html",
		TIMEOUT	// in milliseconds
	);
};

d('lsnmp').onclick = function() {
	//event.preventDefault();
	
	changeBtnColor(this, 'pure-button-disabled' ,2500);
	tinyxhr('orden.cgi?ivar=6&value=3',
		function (err,data,xhr){ 
			if (err){
				console.log("Leer SNMP Defecto Failed! ",err,'status='+xhr.status);
			}
			else{
				console.log("Leer SNMP Defecto Success");
				network_ajax.restart();
			}
		},
		"GET",
		null,
		"html",
		TIMEOUT	// in milliseconds
	);
};

d('ldip').onclick = function() {
		//event.preventDefault();
	changeBtnColor(this, 'pure-button-disabled' ,2500);
	tinyxhr('orden.cgi?ivar=6&value=0',
		function (err,data,xhr){ 
			if (err){
				console.log("goterr ",err,'status='+xhr.status);
				console.log("Leer IP Defecto Failed!");
			}
			else{
				console.log("Leer IP Defecto Success");
				//Coger IP nueva
				redirect = 'http://' + q('input[name="dip"]').value;
				d('rebootaddr').innerHTML = redirect;
				d('rebootaddr').attributes['href'].value = redirect;
				d('modal_ipcfg').style.display = '';
				countDown(6);
			}
		},
		"GET",
		null,
		"html",
		TIMEOUT	// in milliseconds
	);
};

d('ipcfg').onsubmit = function(e) {
	// stop form from submitting normally 
	//event.preventDefault();
	var e = e || window.event; // IE compatibility
	(e.preventDefault) ? e.preventDefault() : e.returnValue = false;

	//var values = $(this).serialize();
	var values = serializeForm('ipcfg');

	changeBtnColor(q('#gdip'), 'pure-button-disabled' ,2500);
	
	tinyxhr('setNet.htm',
		function (err,data,xhr){ 
			if (err){
				console.log("Network Failed ",err,'status='+xhr.status);
			}
			else{
				console.log("Network Success");

				//Coger IP nueva
				redirect = 'http://' + q('input[name="rip"]').value;
				d('rebootaddr').innerHTML = redirect;
				d('rebootaddr').attributes['href'].value = redirect;
				
				//Abrir ventana Modal 
				d('modal_ipcfg').style.display = '';
				
				//enviar reboot.cgi
				tinyxhr('reboot.cgi',
					function (err,data,xhr){ 
						if (err){
							console.log("goterr ",err,'status='+xhr.status);
							console.log("Reboot Failed");
						}
						else{
							console.log("Reboot Success");
						}
					},
					"GET",
					null,
					"html",
					TIMEOUT	// in milliseconds
				);
				
				countDown(6);
			}
		},
		"POST",
		values,
		null,
		TIMEOUT	// in milliseconds
	);
};

d('ntpcfg').onsubmit = function(e) {
	// stop form from submitting normally
	var e = e || window.event;
	(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
	var values = serializeForm('ntpcfg');
	changeBtnColor(q('#gntp'), 'pure-button-disabled' ,2500);
	tinyxhr('setSNTP.htm',
		function (err,data,xhr){ 
			if (err){
				console.log("NTP Failed ",err,'status='+xhr.status);
			}
			else{
				console.log("NTP Success");
				network_ajax.restart();
			}
		},
		"POST",
		values,
		null,
		TIMEOUT
	);
};

/*
d('pswcfg').onsubmit = function(e) {
	// stop form from submitting normally
	var e = e || window.event;
	(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
	var values = serializeForm('pswcfg');
	changeBtnColor(q('#gpsw'), 'pure-button-disabled' ,2500);
	tinyxhr('setPSW.htm',
		function (err,data,xhr){ 
			if (err){
				console.log("PSW Failed ",err,'status='+xhr.status);
			}
			else{
				console.log("PSW Success");
				network_ajax.restart();
			}
		},
		"POST",
		values,
		null,
		TIMEOUT
	);
};
*/

d('ldapcfg').onsubmit = function(e) {
	// stop form from submitting normally
	var e = e || window.event;
	(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
	var values = serializeForm('ldapcfg');
	changeBtnColor(q('#gldap'), 'pure-button-disabled' ,2500);
	tinyxhr('setLDAP.htm',
		function (err,data,xhr){ 
			if (err){
				console.log("LDAP Failed ",err,'status='+xhr.status);
			}
			else{
				console.log("LDAP Success");
				network_ajax.restart();
				//
				events_ajax.restart();
				//
			}
		},
		"POST",
		values,
		null,
		TIMEOUT
	);
};

d('loccfg').onsubmit = function(e) {
	// stop form from submitting normally
	var e = e || window.event;
	(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
	var values = serializeForm('loccfg');
	changeBtnColor(q('#gloc'), 'pure-button-disabled' ,2500);
	tinyxhr('setLOC.htm',
		function (err,data,xhr){ 
			if (err){
				console.log("LOC Failed ",err,'status='+xhr.status);
			}
			else{
				console.log("LOC Success");
				equipo_ajax.restart();
			}
		},
		"POST",
		values,
		null,
		TIMEOUT
	);
};

d('snmpcfg').onsubmit = function(e) {
	// stop form from submitting normally
	var e = e || window.event;
	(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
	var values = serializeForm('snmpcfg');
	changeBtnColor(q('#gsnmp'), 'pure-button-disabled' ,2500);
	tinyxhr('setSNMP.htm',
		function (err,data,xhr){ 
			if (err){
				console.log("SNMP Failed ",err,'status='+xhr.status);
			}
			else{
				console.log("SNMP Success");
				//network_ajax.restart();
				setTimeout(function() {
					network_ajax.restart();
				},5000);
				
			}
		},
		"POST",
		values,
		null,
		TIMEOUT	// in milliseconds
	);
};

//$(document).ready(function() {
addListener(window, 'load', function() {

	network_ajax = periodic({period: PERIODO, decay: 1, max_period: 60000}, function() {
		if(xhr_network && xhr_network.readystate != 4){
			xhr_network.abort();
		}
		
		xhr_network = tinyxhr('network.xml',
			function (err,data,xhr){ 
				if (err){
					console.log("Network Failed ",err,'status='+xhr.status);
				}
				else{
					console.log("Network Success");
					updateNetwork(xhr.responseXML);
				}
				
				network_ajax.cancel();
				
			},
			"GET",
			null,
			"xml",
			TIMEOUT	// in milliseconds
		);
	});
	
	equipo_ajax = periodic({period: PERIODO, decay: 1, max_period: 60000}, function() {
		if(xhr_equipo && xhr_equipo.readystate != 4){
			xhr_equipo.abort();
		}
		
		xhr_equipo = tinyxhr('equipo.xml',
			function (err,data,xhr){ 
				if (err){
					console.log("Equipo Failed ",err,'status='+xhr.status);
				}
				else{
					console.log("Equipo Success");
					updateEquipo(xhr.responseXML);
				}
				
				equipo_ajax.cancel();
				
			},
			"GET",
			null,
			"xml",
			TIMEOUT	// in milliseconds
		);
	});
	
});

/*<!-- Help-->*/

/*<!-- Boot-->*/

function paso(id,p) {

	var pid = id + '_p';
		
	switch(p) {
		case 1:
			q(pid + '1').className = 'red';
			q(pid + '2').className = 'blue';
			q(pid + '3').className = 'blue';
			break;
		case 2:
			q(pid + '1').className = 'orange';
			q(pid + '2').className = 'red';
			q(pid + '3').className = 'blue';
			break;
		case 3:
			q(pid + '1').className = 'orange';
			q(pid + '2').className = 'orange';
			q(pid + '3').className = 'red';
			break;
		case 4:
			q(pid + '1').className = 'orange';
			q(pid + '2').className = 'orange';
			q(pid + '3').className = 'orange';
			break;
		default:
			q(pid + '1').className = '';
			q(pid + '2').className = '';
			q(pid + '3').className = '';
			break;
	} 
}

if (!!window.FileReader){
	var fileReader = new FileReader();
	var fileReaderCfg = new FileReader();
}

function updateBoot(xmlData) {
	if(!xmlData)
	{
		return;
	}
	//q('#Vhard').textContent = getXMLValue(xmlData,'hard');
	//q('#Vboot').textContent = getXMLValue(xmlData,'boot');
	//q('#Vapp').textContent = getXMLValue(xmlData,'app');
	//q('#Vappl').textContent = getXMLValue(xmlData,'appl');

	q('#Vboot').textContent = getAXMLValue(xmlData,'REV_FW_SW','boot');
	q('#Vapp').textContent = getAXMLValue(xmlData,'REV_FW_SW','ver_sw');
	q('#Ind').textContent = getAXMLValue(xmlData,'REV_FW_SW','ident_soft');
	q('#Vappl').textContent = getAXMLValue(xmlData,'REV_FW_SW','app_load');
	
	//if ((parseFloat( getXMLValue(xmlData,'app') )) == (parseFloat( getXMLValue(xmlData,'appl') ))){
	if ((parseFloat( getAXMLValue(xmlData,'REV_FW_SW','ver_sw') )) == (parseFloat( getAXMLValue(xmlData,'REV_FW_SW','app_load') ))){	
		removeClass(q('#Vappl').parentNode,'r');
		addClass(q('#Vappl').parentNode, 'g');
	}
	else{
		removeClass(q('#Vappl').parentNode,'g');
		addClass(q('#Vappl').parentNode, 'r');
	}
	
	//q('.md5r').textContent = getXMLValue(xmlData,'md5_firm');
	if (q('.md5l').value != ''){
		q('.md5r').textContent = getAXMLValue(xmlData,'REV_FW_SW','md5_firm');
	}
	
	//var boot_cond = parseInt(getXMLValue(xmlData,'cond'));
	var boot_cond = parseInt(getAXMLValue(xmlData,'REV_FW_SW','cond'));
	
	if (boot_cond == 1){
		q('#Cond').textContent = 'SI';
		removeClass(q('#Cond').parentNode,'r');
		addClass(q('#Cond').parentNode, 'g');
	}
	else if (boot_cond == 0){
		q('#Cond').textContent = 'NO (Ausencia de red)';
		removeClass(q('#Cond').parentNode,'g');
		addClass(q('#Cond').parentNode, 'r');
	}
	else if (boot_cond == 2){
		q('#Cond').textContent = 'NO (Firmware incompatible)';
		removeClass(q('#Cond').parentNode,'g');
		addClass(q('#Cond').parentNode, 'r');
	}
	
	if ((q('.md5l').value.replace(/\s+/g, '') == q('.md5r').textContent.replace(/\s+/g, ''))){
		removeClass(q('.md5r').parentNode,'r');
		addClass(q('.md5r').parentNode, 'g');
		if (boot_cond == 1){
			removeClass(q('#update'),'pure-button-disabled');
			//addClass(q('#boot_p3'),'red');
			paso('#boot',3);
		}
		else{
			addClass(q('#update'),'pure-button-disabled');
			//removeClass(q('#boot_p3'),'red');
			paso('#boot',2);
		}
	}
	else{
		addClass(q('#update'),'pure-button-disabled');
		//removeClass(q('#boot_p3'),'red');
		//paso(2);
		removeClass(q('.md5r').parentNode,'g');
		addClass(q('.md5r').parentNode, 'r');
	}
	
	//q('.cfgmd5r').textContent = getXMLValue(xmlData,'md5_cfg');
	q('.cfgmd5r').textContent = getAXMLValue(xmlData,'REV_CONF','md5_cfg');
	
	q('#CfgVer').textContent = getAXMLValue(xmlData,'REV_CONF','ver_conf');
	q('#CfgDate').textContent = getAXMLValue(xmlData,'REV_CONF','date_conf');
	
	if (q('.cfgmd5l').value.replace(/\s+/g, '') == q('.cfgmd5r').textContent.replace(/\s+/g, '')){
		removeClass(q('#cfg_update'),'pure-button-disabled');
		removeClass(q('.cfgmd5r').parentNode,'r');
		addClass(q('.cfgmd5r').parentNode, 'g');
		paso('#cfg',3);
	}
	else{
		addClass(q('#cfg_update'),'pure-button-disabled');
		removeClass(q('.cfgmd5r').parentNode,'g');
		addClass(q('.cfgmd5r').parentNode, 'r');
	}
	
}

function commafy(a) {
	var b = a.toString().split(".");
	if (b[0].length >= 5) {
		b[0] = b[0].replace(/(\d)(?=(\d{3})+$)/g, "$1,")
	}
	if (b[1] && b[1].length >= 5) {
		b[1] = b[1].replace(/(\d{3})/g, "$1 ")
	}
	return b.join(".")
}

if (!!window.FileReader){
	fileReader.onload = function (event) {
		//http://ibnrubaxa.blogspot.com.es/2012/10/ie10-fileapi.html
		var base64 = event.target.result.replace(/^data:[^,]+,/, '');
		var data = window.atob(base64); // bingo!				
		//var data 		= toBin(event.target.result);
		//var data 		= rstr2binl(event.target.result);
		var md5_result 	= bin2hex(rstr_md5(data));
		q('.md5l').value = md5_result;
		removeClass(q('#upload'),'pure-button-disabled');
		//addClass(q('#boot_p2'),'red');
		paso('#boot',2);
		hideLoader();
	};
	
	fileReaderCfg.onload = function (event) {
		var base64 = event.target.result.replace(/^data:[^,]+,/, '');
		var data = window.atob(base64); // bingo!				
		var md5_result 	= bin2hex(rstr_md5(data));
		q('.cfgmd5l').value = md5_result;
		removeClass(q('#cfg_upload'),'pure-button-disabled');
		paso('#cfg',2);
		hideLoader();
	};	
}

function bin2hex (s){
	// Converts the binary representation of data to hex  
	// 
	// version: 909.322
	// discuss at: http://phpjs.org/functions/bin2hex
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   bugfixed by: Onno Marsman
	// +   bugfixed by: Linuxworld
	// *     example 1: bin2hex('Kev');
	// *     returns 1: '4b6576'
	// *     example 2: bin2hex(String.fromCharCode(0x00));
	// *     returns 2: '00'
	var i,j, f = 0, a = [];
	
	s += '';
	f = s.length;
	
	for (i = 0,j = 0; i<f; i++,j++) {
		a[j] = s.charCodeAt(i).toString(16).replace(/^([\da-f])$/,"0$1").toUpperCase();
		if((i & 0x03) == 3){
			j++;
			a[j] = ' ';
		}
	}
	
	return a.join('');
}

//$(document).ready(function() {
addListener(window, 'load', function() {

	boot_ajax = periodic({period: (PERIODO*2), decay: 1, max_period: 60000}, function() {
		if(xhr_boot && xhr_boot.readystate != 4){
			xhr_boot.abort();
		}
		
		xhr_boot = tinyxhr('boot.xml',
			function (err,data,xhr){ 
				if (err){
					console.log("Boot Failed ",err,'status='+xhr.status);
				}
				else{
					console.log("Boot Success");
					updateBoot(xhr.responseXML);
				}
				
				boot_ajax.cancel();
				
			},
			"GET",
			null,
			"xml",
			TIMEOUT*2	// in milliseconds
		);
	});
	
	var file = null; // FileList object
	var file_cfg = null; // FileList object
	
	addListener(q('input[id=file]'), 'click', function() {
		this.value = '';
	});

	q('input[id=file]').onchange = function(evt) {
		//$('#pretty-input').val($(this).val().replace("C:\\fakepath\\", ""));
		//q('#pretty-input').value = this.value.replace("C:\\fakepath\\", "");
		
		var evt = evt || window.event; // IE compatibility
		
		var filename;
		if (window.FileList){
			//var file = evt.target.files[0]; // FileList object
			file = evt.target.files[0]; // FileList object
			filename = file.name;
		}
		else{
			file = null;
			filename = evt.srcElement.value.replace("C:\\fakepath\\", "");
		}
		
		//q("#upload-file-info").innerHTML = filename;
		
		q("#pretty-input").value = filename;
		
		addClass(q('#upload'),'pure-button-disabled');
		addClass(q('#update'),'pure-button-disabled');
		paso('#boot',1);
		q('.md5r').textContent = '--';
		removeClass(q('.md5r').parentNode,'g');
		addClass(q('.md5r').parentNode, 'r');
		
		//var file = evt.target.value;
		
		// Only process bin files.
		if (!filename.match('\.bin')) { 
			//$('#upload').addClass('disabled');
			//addClass(q('#upload'), "disabled");
			
			//$('#form_upload').resetForm();
			alert("Debe seleccionar un fichero .bin válido!"); 
			return; 
		}
		
		q('#file_name_label').textContent = filename;
		
		//q('#loading').innerHTML = 'Progreso';
		q('#bar_upload').style.width = '0%';
		q('#percent_upload').textContent = '0%';
		q('#status_upload').innerHTML = '';
		q('#speed_upload').textContent = '';
		
		if (file){
			//$('#file_name_label').text( file.name );
			//$('#file_size_label').text( commafy(file.size) + ' Bytes');
			
			//q('#file_name_label').textContent = file.name;
			q('#file_size_label').textContent = commafy(file.size) + ' Bytes';
			
			//$('#boot_id').showLoader();
			showLoader(d('boot_id'));
			
			fileReader.readAsDataURL(file);
		}
		else{
			q('#file_size_label').textContent = 'Se recomienda uilizar un navegador moderno';
			//File API http://caniuse.com/fileapi
			q('.md5l').value = 'Copie el valor de la suma md5 del fichero';
			removeClass(q('#upload'),'pure-button-disabled');
			paso('#boot',2);
		}
	};
	
	addListener(q('input[id=fich_pub]'), 'click', function() {
		this.value = '';
	});
	
	q('input[id=fich_pub]').onchange = function(evt) {
		var evt = evt || window.event; // IE compatibility
		
		var filename;
		if (window.FileList){
			file_cfg = evt.target.files[0]; // FileList object
			filename = file_cfg.name;
		}
		else{
			file_cfg = null;
			filename = evt.srcElement.value.replace("C:\\fakepath\\", "");
		}
		
		q("#cfg_input").value = filename;
		
		addClass(q('#cfg_upload'),'pure-button-disabled');
		addClass(q('#cfg_update'),'pure-button-disabled');
		paso('#cfg',1);
		q('.cfgmd5r').textContent = '--';
		removeClass(q('.cfgmd5r').parentNode,'g');
		addClass(q('.cfgmd5r').parentNode, 'r');
		
		// Only process csv files.
		if (!filename.match('\.csv')&&!filename.match('\.xml')) { 
			//addClass(q('#cfg_upload'), "disabled");
			//$('#cfg_form_upload').resetForm();
			alert("Debe seleccionar un fichero .csv válido!"); 
			return; 
		}
		
		q('#cfg_file_name').textContent = filename;
		
		//q('#cfg_loading').innerHTML = 'Progreso';
		q('#cfg_bar').style.width = '0%';
		q('#cfg_percent').textContent = '0%';
		q('#cfg_status').innerHTML = '';
		
		if (file_cfg){
			q('#cfg_file_size').textContent = commafy(file_cfg.size) + ' Bytes';
			showLoader(d('settings_upload_tab'));
			fileReaderCfg.readAsDataURL(file_cfg);
		}
		else{
			q('#cfg_file_size').textContent = 'Se recomienda uilizar un navegador moderno';
			//File API http://caniuse.com/fileapi
			q('.cfgmd5l').value = 'Copie el valor de la suma md5 del fichero';
			removeClass(q('#cfg_upload'),'pure-button-disabled');
			paso('#cfg',2);
		}
	};

	q('#upload').onclick = function(e) {
		if (hasClass(this,'pure-button-disabled')) { return false; }
		// stop form from submitting normally 
		var e = e || window.event; // IE compatibility
		(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
		
		if (file){
			tinyxhr('/upgrade',
				function (err,data,xhr){ 
					if (err){
						console.log("Upload Failed ",err,'status='+xhr.status);
					}
					else{
						console.log("Upload Success");
					}
					
					var percentVal = '100%';

					q('#bar_upload').style.width = percentVal;
					q('#percent_upload').textContent = percentVal;						
					//q('#loading').textContent = "Subido";
					q('#status_upload').innerHTML = xhr.responseText;
					
					boot_ajax.restart();
				},
				"POST",
				q('#form_upload input[type=file]').files[0],
				'multipart/form-data',
				0	// in milliseconds
			);
		}
		else{
			// Submit the form...
			//this.submit();
			//q('#form_upload').submit();
			showLoader(q('#boot_id .tab-content'));
			fileUpload(q('#form_upload'),'/upgrade','status_upload');
		}
	};

	q('#cfg_upload').onclick = function(e) {
		if (hasClass(this,'pure-button-disabled')) { return false; }	
		// stop form from submitting normally 
		var e = e || window.event; // IE compatibility
		(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
		
		q('.cfgmd5r').textContent = '';
		removeClass(q('.cfgmd5r').parentNode,'g');
		addClass(q('.cfgmd5r').parentNode, 'r');
		
		events_ajax.cancel();
		
		if (file_cfg){
			tinyxhr('/upgrade',
				function (err,data,xhr){ 
					if (err){
						console.log("Upload Cfg Failed ",err,'status='+xhr.status);
					}
					else{
						console.log("Upload Cfg Success");
					}
					
					var percentVal = '100%';

					q('#cfg_bar').style.width = percentVal;
					q('#cfg_percent').textContent = percentVal;						
					//q('#cfg_loading').textContent = "Subido";
					q('#cfg_status').innerHTML = xhr.responseText;
					
					boot_ajax.restart();
				},
				"POST",
				q('#cfg_form_upload input[type=file]').files[0],
				'multipart/form-data',
				0	// in milliseconds
			);
		}
		else{
			// Submit the form...
			//this.submit();
			//q('#form_upload').submit();
			showLoader(q('#settings_upload_tab .tab-content'));
			fileUpload(q('#cfg_form_upload'),'/upgrade','cfg_status');
		}
	};
	
	q('#update').onclick = function() {
		if (hasClass(this,'pure-button-disabled')) { return false; }
		d('modal4').style.display = '';	
	}
	
	//q('#update').onclick = function() {
	q('#btn_boot').onclick = function() {
	
		d('modal4').style.display = 'none';
	
		//if ($(this).hasClass('disabled')) { return false; }
		//if (hasClass(this,'pure-button-disabled')) { return false; }
		
		//tinyxhr('orden.cgi?ivar=7&value=0',
		tinyxhr('upgrade.cgi?firm',
			function (err,data,xhr){ 
				if (err){
					console.log("Boot Update Failed!");
					console.log("goterr ",err,'status='+xhr.status);
					//q('#status_upload').innerHTML = "Orden de Actuzalización no recibida";
				}
				else{
					console.log("Boot Update Success");
					//q('#status_upload').innerHTML = "En proceso de Actualización de Firmware...";
				}
			},
			"GET",
			null,
			"html",
			TIMEOUT	// in milliseconds
		);
		
		q('#status_upload').innerHTML = "En proceso de Actualización de Firmware...";
		
		paso('#boot',4);
		
		//$('#loading').html("Updating...");
		//d('loading').innerHTML = "Actualizando...";
		seconds_ini = Math.floor(new Date().getTime() / 1000);
		
		var percentVal = 0;
		//bar.width(percentVal + "%");
		//percent.html(percentVal + "%");
		//speed_bar.html('');
		//bar.style.width = percentVal + '%';
		//percent.textContent = percentVal + '%';
		//speed_bar.textContent = '';
		q('#bar_upload').style.width = percentVal + '%';
		q('#percent_upload').textContent = percentVal + '%';
		q('#speed_upload').textContent = '';
		//q('#loading').innerHTML = "Actualizando...";
							
		var progress = setInterval(function() {
			seconds_act = Math.floor(new Date().getTime() / 1000);
			SessionRestart();
			if (percentVal >= 45) {
				clearInterval(progress);
				
				setTimeout(function() {
					window.location = 'login.html';
				}, 2000);
			}
			else {
				if (percentVal >= 40) {
					//q('#loading').innerHTML = "Redireccionando...";
					q('#status_upload').innerHTML = "El nuevo Firmware ha sido actualizado Correctamente...Redireccionando";
				}
				else if (percentVal >= 30) {
					//q('#loading').innerHTML = "Programando uC...";
					q('#status_upload').innerHTML = "En proceso de Actualización de Firmware...Programando uC";
				}
				else if (percentVal >= 25) {
					//q('#loading').innerHTML = "Borrando uC...";
					q('#status_upload').innerHTML = "En proceso de Actualización de Firmware...Borrando uC";
				}
				else{
					//q('#loading').innerHTML = "Volcando Flash...";
					q('#status_upload').innerHTML = "En proceso de Actualización de Firmware...Volcando Flash";
				}
				
				percentVal = percentVal + 1;
				//bar.width(percentVal + "%");
				//bar.style.width = percentVal + '%';
				
				q('#bar_upload').style.width = Math.floor(percentVal * 100 / 45) + '%';
			}
			//percent.html(percentVal + "%");
			//percent.textContent = percentVal + '%';
			q('#percent_upload').textContent = Math.floor(percentVal * 100 / 45) + '%';
			//speed_bar.html('Time:'+ (seconds_act - seconds_ini) + 'sec');
			//speed_bar.textContent = 'Tiempo:'+ (seconds_act - seconds_ini) + 'sec';
			q('#speed_upload').textContent = 'Tiempo:'+ (seconds_act - seconds_ini) + 'sec';
		}, 1000);

		return false;
	}; 
	
	q('#cfg_update').onclick = function() { 
	
		//if ($(this).hasClass('disabled')) { return false; }
		if (hasClass(this,'pure-button-disabled')) { return false; }
	
		//tinyxhr('orden.cgi?ivar=7&value=1',
		tinyxhr('upgrade.cgi?conf',
			function (err,data,xhr){ 
				if (err){
					console.log("Fich Update Failed!");
					console.log("goterr ",err,'status='+xhr.status);
					//q('#cfg_status').innerHTML = "Error en la actualización del Fichero";
				}
				else{
					console.log("Fich Update Success");
					//q('#cfg_status').innerHTML = "El Fichero ha sido actualizado Correctamente";
				}
			},
			"GET",
			null,
			"html",
			TIMEOUT	// in milliseconds
		);
		
		q('#cfg_status').innerHTML = "El Fichero ha sido actualizado";
		
		paso('#cfg',4);
	
		//$('#cfg_loading').html("Updated");
		//d('cfg_loading').innerHTML = "Actualizado";
		return false;
	};
	
});

/*<!-- General-->*/

function logout(arg){
	DeleteCookie("language");
	DeleteCookie("username");
	DeleteCookie("profile");
	if (arg){
		window.location = 'login.html?arg=' + arg;
	}
	else{
		window.location = 'login.html';
	}
}

var tickDuration = 1000;
var SessionTime = 10 * 60 * tickDuration;
var SessionTimeleft = SessionTime;
var myInterval;
var myTimeOut;

function SessionExpireEvent()
{ 
	clearInterval(myInterval);
	//alert("Session expired");
	logout('LA SESION HA EXPIRADO!');
}

function SessionRestart()
{
	clearTimeout(myTimeOut);
	SessionTimeleft = SessionTime;
	d('session').textContent = Math.floor(SessionTimeleft/tickDuration/60) + ' min: ' + Math.floor((SessionTimeleft / tickDuration)%60) + ' seg';
	myTimeOut = setTimeout(SessionExpireEvent,SessionTime);	
}

var node = null;
var tab = null;
var arg = null;
var back = false;

//$(document).ready(function() {
//window.onload = function() {
//document.addEventListener('DOMContentLoaded', function() {
addListener(window, 'load', function() {

	if (GetCookie('profile') == 'ZMS') {
		//q('#optionsRadios2').checked = true;
		q('#usericon').className = 'uni icn-wrench';
		q("#username").textContent = ' ZMS';
		
		//q('a[href="calibracion.csv"]').parentNode.parentNode.style.display = '';
		q('#cal').style.display = '';
		q('a[href="#boot"]').style.display = '';
		q('a[href="#settings_his_tab"]').style.display = '';
		q('a[href="PSU_mod.csv"]').style.display = '';
		q('a[href="#alarms_cfg_tab"]').style.display = '';
		q('a[href="#equipo_psw_tab"]').style.display = '';
		q('#btn_demoModal3').disabled = false;
		q('#btn_demoModal2').disabled = false;
		
		Array.prototype.forEach.call(qa('.zms'), function(e) {removeClass(e,'hide');});
		
		q('#ldapcfg input[name=bdap]').disabled = false;
		q('#ldapcfg input[name=sdap]').disabled = false;
		q('#ldapcfg textarea[name=fdap]').disabled = false;
	}
	else if (GetCookie('profile') == 'ADMIN') {
		//q('#optionsRadios2').checked = true;
		q('#usericon').className = 'uni icn-wrench';
		q("#username").textContent = GetCookie('username');
		
		//q('a[href="calibracion.csv"]').parentNode.parentNode.style.display = 'none';
		q('#cal').style.display = 'none';
		q('a[href="#boot"]').style.display = '';					
		q('a[href="#settings_his_tab"]').style.display = '';
		q('a[href="PSU_mod.csv"]').style.display = '';
		q('a[href="#alarms_cfg_tab"]').style.display = 'none';
		q('a[href="#equipo_psw_tab"]').style.display = '';
		q('#btn_demoModal3').disabled = true;
		q('#btn_demoModal2').disabled = false;
		
		q('#ldip').disabled = true;
		q('#lldap').disabled = true;
		q('#lntp').disabled = true;
		q('#lsnmp').disabled = true;
		q('#lloc').disabled = true;
	}
	else{
		//q('#optionsRadios1').checked = true;
		q('#usericon').className = 'uni icn-search';
		q("#username").textContent = GetCookie('username');

		Array.prototype.forEach.call(qa('#network_id input,#network_id select,#network_id .pure-button'), function(el) {
			el.disabled = true;
		});
		
		Array.prototype.forEach.call(qa('#equipo_id input,#equipo_id select,#equipo_id .pure-button'), function(el) {
			el.disabled = true;
		});
		
		Array.prototype.forEach.call(qa('#cfg_form_upload input,#cfg_form_upload .pure-button'), function(el) {
			el.disabled = true;
		});		
				
		//q('a[href="calibracion.csv"]').parentNode.parentNode.style.display = 'none';
		q('#cal').style.display = 'none';
		q('a[href="#boot"]').style.display = 'none';
		q('a[href="#settings_his_tab"]').style.display = 'none';
		q('a[href="PSU_mod.csv"]').style.display = 'none';
		q('a[href="#alarms_cfg_tab"]').style.display = 'none';
		q('a[href="#equipo_psw_tab"]').style.display = 'none';
		q('#btn_demoModal3').disabled = true;
		q('#btn_demoModal2').disabled = true;
	}

	myInterval = setInterval(function(){
		SessionTimeleft = SessionTimeleft - tickDuration;
		//$("label").text(SessionTime);
		//d('session').textContent = SessionTimeleft/tickDuration + ' seg';
		d('session').textContent = Math.floor(SessionTimeleft/tickDuration/60) + ' min: ' + Math.floor((SessionTimeleft / tickDuration)%60) + ' seg';
	},tickDuration);
	
	
	myTimeOut = setTimeout(SessionExpireEvent,SessionTime);

	
	/*
	$("input").click(function(){
		clearTimeout(myTimeOut);
		SessionTime = 10000;
		myTimeOut = setTimeout(SessionExpireEvent,SessionTime);
	});
	*/
	
	var menu = d('menu'),
		menuLink = d('menuLink'),
		layout = d('layout');

	menuLink.onclick = function (e) {
		e.preventDefault();
		var active = 'active';
		toggleClass(layout, active);
		toggleClass(menu, active);
		toggleClass(menuLink, active);
	};
	
	//var arg = 'e=sis';
	
	//originalHash = window.location.hash;
	originalHash = '#null';
	
	//$(window).bind('hashchange', function() {
	function hashChanged(event){
		//var newHash = window.location.hash;
		var newHash = window.location.hash ? window.location.hash : '#dashboard';

		var split = newHash.split('?');
		
		//Node
		node = newHash.split(/[_?]/)[0];
		
		//Tiene arg
		if (newHash.split('?').length > 1){
			arg = newHash.split('?')[1];
		}
		else{
			arg = null;
		}
		
		//Tiene tab
		if (newHash.split('_').length > 1){
			tab = newHash.split('_')[1].split('?')[0];
		}
		else{
			tab = null;
		}
		
		//Cambio de node
		if (newHash.split(/[_?]/)[0] != originalHash.split(/[_?]/)[0]){
			//Tiene tab
			if (newHash.split('_').length > 1){
				var cid = newHash.split(/[_?]/)[0] + '_id';
				var nav_sec = qa(cid + ' .navs a[href]')[0].hash.split('_tab')[0];
			}
			else{
				var nav_sec = newHash.split(/[_?]/)[0];
			}
			
			if (qa('#menu li a[href="'+nav_sec+'"]').length){
				var myeve = document.createEvent("HTMLEvents");
				myeve.initEvent("click", true, true);
				back = true;
				q('#menu li a[href="'+nav_sec+'"]').dispatchEvent(myeve);
			}
		}
		//Cambio de tab
		else if (((newHash.split('_').length > 1)&&(originalHash.split('_').length > 1))&&
				(newHash.split('_')[1].split('?')[0] != originalHash.split('_')[1].split('?')[0] )){

				var cid = newHash.split(/[_?]/)[0] + '_id';
				var nav_sec = newHash.split('?')[0] + '_tab';
				var myeve = document.createEvent("HTMLEvents");
				myeve.initEvent("click", true, true);
				back = true;
				q(cid + ' .navs a[href="'+nav_sec+'"]').dispatchEvent(myeve);
		}
		// Cambio de arg
		else if (((newHash.split('?').length > 1)&&(originalHash.split('?').length > 1))&&
				(newHash.split('?')[1] != originalHash.split('?')[1])){
				var li = newHash + '_li';
				var myeve = document.createEvent("HTMLEvents");
				myeve.initEvent("click", true, true);
				back = true;
				q('li a[href="'+li+'"]').dispatchEvent(myeve);
		}
	};
	//});

	if(window.addEventListener) {
		window.addEventListener("hashchange", hashChanged, false);
	}
	else if (window.attachEvent) {
		window.attachEvent("onhashchange", hashChanged);//SEE HERE...
		//missed the on. Fixed thanks to @Robs answer.
	}

	/*
	d('loginform').onclick = function(e) {
		var e = e || window.event; // IE compatibility
		//evt.stopPropagation();
		
		if (e && e.stopPropagation) //if stopPropagation method supported
			e.stopPropagation();
		else
			e.cancelBubble = true;
	};
	*/
	
	d('logout').onclick = function() { 
		logout();	
	};
	
	/*
	d('sign-in').onclick = function() { 
		d('uid').value = d('uid').value.toLowerCase();
		d('pwd').value = d('pwd').value.toLowerCase();

		SetCookie("language", "es");
		SetCookie("username", d('uid').value.toUpperCase());
		
		if (d('uid').value.toUpperCase()=='ZMS'){
			SetCookie("profile","ZMS");
		}
		else{
			if (document.getElementsByName("rol")[1].checked){
				SetCookie("profile","ADMIN");
			}
			else{
				SetCookie("profile","USER");
			}
		}
		
		d('loginform').submit();
	};
	
	Array.prototype.forEach.call(qa('#uid,#pwd'), function(el) {
		el.onkeypress = function(e) { 
			var e = e || window.event; // IE compatibility
			if (e.keyCode == 13) {
				if (e.preventDefault) { e.preventDefault(); } else { e.returnValue = false; } 
					//d('sign-in').click();
					var myeve = document.createEvent("HTMLEvents");
					myeve.initEvent("click", true, true);
					d('sign-in').dispatchEvent(myeve);
			}
		};
	});
	*/

	//nav-secondary
	Array.prototype.forEach.call(qa('.navs a'), function(el) {
		//el.onclick = function(e,a) {
		el.onclick = function(e) {
		var e = e || window.event; // IE compatibility
		(e.preventDefault) ? e.preventDefault() : e.returnValue = false;

		var a = arg;

		//
		/*
		if (this.hash == '#settings_cfg_tab'){
			if(a == undefined){
				a = 'e=sis';
			}
		}
		else{
			a = undefined;
		}
		arg = a;
		*/
		//
		
		if (back != true){
			node = this.hash.split(/[_?]/)[0];
			if (this.hash.split('_').length > 1){
				tab = this.hash.split('_')[1].split('?')[0];
			}
			else{
				tab = null;
			}
			//if (node == '#settings')
			if (this.hash == '#settings_cfg_tab'){
				arg = 'e=sis';
			}
			//if (arg == null){
			else if (this.hash.split('?').length > 1){
				arg = this.hash.split('?')[1];
			}
			else{
				arg = null;
			}
			//}
		}
		
		var cid = this.hash.split('_')[0] + '_id';
		
		var tabs = qa(cid + ' .navs a');
		var ns = this;
		Array.prototype.forEach.call(tabs, function(a) {
			if (a.getAttribute('href') != ns.hash){
				removeClass(a,'btn-b');
				addClass(a,'btn-w');
			}
			else{
				addClass(a,'btn-b');
				removeClass(a,'btn-w');
			}
		});
		
		//Array.prototype.forEach.call(qa('.navs a.btn-b'), function(e) {removeClass(e,'btn-b'); addClass(e,'btn-w')});
		//this.className += ' btn-b';
		
		//tab
		var tabs = qa(cid + ' .tab');
		Array.prototype.forEach.call(tabs, function(e) {addClass(e, 'hide')});
		//qa(this.hash)[0].className += ' active';
		removeClass(qa(this.hash)[0],'hide');
		
		flushThis(this.hash);
		//addClass(document.body,'active');
		
		qa('.breadcrumb span')[2].textContent = " › " + this.childNodes[1].textContent;
		qa('.breadcrumb span')[3].textContent = '';
		
		if(arg == undefined) {
			originalHash = this.getAttribute('href').split('_tab')[0];
			location.hash = originalHash;
		}
		else{
			originalHash = this.getAttribute('href').split('_tab')[0] + '?' + arg;
			location.hash = originalHash;
		}

		if (cid == '#alarms_id'){
			events_ajax.restart();
			
			if (this.hash == "#alarms_act_tab"){
				ielem = ielem_ant = 6;
				index = index_ant = 0;
				removeRows('alarms_table');
				
				alarms_ajax.restart();
			}
			else if (this.hash == "#alarms_his_tab"){
				d('alarms_his_bar').style.width = '0%';
				d('alarms_his_bar').parentNode.style.display = '';
				d('alarms_his_bar').innerHTML = '0%';
				
				ielem = ielem_ant = 7;
				index = index_ant = 0;							
				removeRows('alarms_his_table');
				
				alarms_ajax.restart();
			}
			else if (this.hash == "#alarms_cfg_tab"){
				//qa('#alarms_config li a').item(0).click();
				var myeve = document.createEvent("HTMLEvents");
				myeve.initEvent("click", true, true);
				qa('#alarms_config li a').item(0).dispatchEvent(myeve);
			}
		}

		if (cid == '#settings_id'){
			events_ajax.restart();
					
			if (this.hash == "#settings_cfg_tab"){
				if(arg == undefined) {
					arg = 'e=sis';
				}
				var li = "#settings_cfg?" + arg + '_li';
				//q('.settings_tree li a[href="'+li+'"]').click();
				var myeve = document.createEvent("HTMLEvents");
				myeve.initEvent("click", true, true);
				q('.settings_tree li a[href="'+li+'"]').dispatchEvent(myeve);
			}
			else if (this.hash == "#settings_his_tab"){
				d('settings_his_bar').style.width = '0%';
				d('settings_his_bar').parentNode.style.display = ''
				
				ielem = ielem_ant = 31;
				index = index_ant = 0;							
				removeRows('settings_his_table');
				settings_his_ajax.restart();
			}
			else{
				//settings_upload_tab
				q("#cfg_input").value = '';
				paso('#cfg',1);
				addClass(q('#cfg_upload'),'pure-button-disabled');
				addClass(q('#cfg_update'),'pure-button-disabled');

				q('#cfg_file_name').textContent = '';
				q('#cfg_file_size').textContent = '0 Bytes';			
				q('.cfgmd5l').value = '';
				q('.cfgmd5r').textContent = '--';
				removeClass(q('.cfgmd5r').parentNode,'g');
				addClass(q('.cfgmd5r').parentNode, 'r');

				q('#cfg_bar').style.width = '0%';
				q('#cfg_percent').textContent = '0%';
				q('#cfg_status').innerHTML = '';
				q('#cfg_speed').textContent = '';
				
				boot_ajax.restart();
			}
		}
		
		back = false;
		
		//return false;
	};
	});
	//});
	
	function ontouch(el, callback){
	 
	 var touchsurface = el,
	 dir,
	 swipeType,
	 startX,
	 startY,
	 distX,
	 distY,
	 threshold = 150, //required min distance traveled to be considered swipe
	 restraint = 100, // maximum distance allowed at the same time in perpendicular direction
	 allowedTime = 500, // maximum time allowed to travel that distance
	 elapsedTime,
	 startTime,
	 handletouch = callback || function(evt, dir, phase, swipetype, distance){}
	 
	 touchsurface.addEventListener('touchstart', function(e){
	  var touchobj = e.changedTouches[0]
	  dir = 'none'
	  swipeType = 'none'
	  dist = 0
	  startX = touchobj.pageX
	  startY = touchobj.pageY
	  startTime = new Date().getTime() // record time when finger first makes contact with surface
	  handletouch(e, 'none', 'start', swipeType, 0) // fire callback function with params dir="none", phase="start", swipetype="none" etc
	  //e.preventDefault()
	 
	 }, false)
	 
	 touchsurface.addEventListener('touchmove', function(e){
	  var touchobj = e.changedTouches[0]
	  distX = touchobj.pageX - startX // get horizontal dist traveled by finger while in contact with surface
	  distY = touchobj.pageY - startY // get vertical dist traveled by finger while in contact with surface
	  if (Math.abs(distX) > Math.abs(distY)){ // if distance traveled horizontally is greater than vertically, consider this a horizontal movement
	   dir = (distX < 0)? 'left' : 'right'
	   handletouch(e, dir, 'move', swipeType, distX) // fire callback function with params dir="left|right", phase="move", swipetype="none" etc
	  }
	  else{ // else consider this a vertical movement
	   dir = (distY < 0)? 'up' : 'down'
	   handletouch(e, dir, 'move', swipeType, distY) // fire callback function with params dir="up|down", phase="move", swipetype="none" etc
	  }
	  //e.preventDefault() // prevent scrolling when inside DIV
	  if (dir == 'left' || dir == 'right')
		e.preventDefault()	  
	 }, false)
	 
	 touchsurface.addEventListener('touchend', function(e){
	  var touchobj = e.changedTouches[0]
	  elapsedTime = new Date().getTime() - startTime // get time elapsed
	  if (elapsedTime <= allowedTime){ // first condition for awipe met
	   if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint){ // 2nd condition for horizontal swipe met
		swipeType = dir // set swipeType to either "left" or "right"
	   }
	   else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint){ // 2nd condition for vertical swipe met
		swipeType = dir // set swipeType to either "top" or "down"
	   }
	  }
	  // Fire callback function with params dir="left|right|up|down", phase="end", swipetype=dir etc:
	  handletouch(e, dir, 'end', swipeType, (dir =='left' || dir =='right')? distX : distY)
	  
	  //e.preventDefault()
	  if (dir == 'left' || dir == 'right')
		e.preventDefault()
	  
	 }, false)
	}
	
	//http://www.javascriptkit.com/javatutors/touchevents3.shtml
	ontouch(d('status_table'), function(evt, dir, phase, swipetype, distance){
	 //if (dir === 'up' || dir === 'down')
	 //	return true;
	 if ((phase == 'end')&&((dir =='right')||(dir =='left'))){
		Array.prototype.forEach.call(qa(".status_tree li a"), function(a,i) {
			if (hasClass(qa(".status_tree li").item(i),'selected')){
				ielem = i%5;
			}
		});
		
		if (dir =='left'){
			if (ielem < 4){
				ielem += 1;
			}
			//ielem = (ielem + 1)%5;			
		}
		else{
			if (ielem > 0){
				ielem -= 1;
			}		
			//ielem = ((ielem - 1)+5)%5;
		}

		//qa(".status_tree li a").item(ielem).click();
		var myeve = document.createEvent("HTMLEvents");
		myeve.initEvent("click", true, true);					
		qa(".status_tree li a").item(ielem).dispatchEvent(myeve);		
	  };
	  //return false;
	});
	
	Array.prototype.forEach.call(qa('.status_tree .r'), function(el) {
		el.onclick = function(e) {
			Array.prototype.forEach.call(qa(".status_tree li a"), function(a,i) {
				if (hasClass(qa(".status_tree li").item(i),'selected')){
					ielem = i%5;
				}
			});
			
			if (hasClass(this,'next')){
				if (ielem < 4){
					ielem += 1;
				}
			}
			else{
				if (ielem > 0){
					ielem -= 1;
				}		
			}
			
			var myeve = document.createEvent("HTMLEvents");
			myeve.initEvent("click", true, true);					
			qa(".status_tree li a").item(ielem).dispatchEvent(myeve);
			
		};
	});
	
	Array.prototype.forEach.call(qa('.settings_tree .r'), function(el) {
		el.onclick = function(e) {
			Array.prototype.forEach.call(qa(".settings_tree li a"), function(a,i) {
				if (hasClass(qa(".settings_tree li").item(i),'selected')){
					ielem = i%5;
				}
			});
			
			if (hasClass(this,'next')){
				if (ielem < 4){
					ielem += 1;
				}
			}
			else{
				if (ielem > 0){
					ielem -= 1;
				}		
			}
			
			var myeve = document.createEvent("HTMLEvents");
			myeve.initEvent("click", true, true);					
			qa(".settings_tree li a").item(ielem).dispatchEvent(myeve);
			
		};
	});
	
	/*
	q('.status_tree .prev').onclick = function(e) {
		//
		var e = e || window.event; // IE compatibility
		(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
		//
		Array.prototype.forEach.call(qa(".status_tree li a"), function(a,i) {
			if (hasClass(qa(".status_tree li").item(i),'selected')){
				ielem = i%5;
			}
		});
		
		if (ielem > 0){
			ielem -= 1;
		}		
		var myeve = document.createEvent("HTMLEvents");
		myeve.initEvent("click", true, true);					
		qa(".status_tree li a").item(ielem).dispatchEvent(myeve);	
	};
	
	q('.status_tree .next').onclick = function(e) {
		//
		var e = e || window.event; // IE compatibility
		(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
		//
		Array.prototype.forEach.call(qa(".status_tree li a"), function(a,i) {
			if (hasClass(qa(".status_tree li").item(i),'selected')){
				ielem = i%5;
			}
		});
		
		if (ielem < 4){
			ielem += 1;
		}		
		var myeve = document.createEvent("HTMLEvents");
		myeve.initEvent("click", true, true);					
		qa(".status_tree li a").item(ielem).dispatchEvent(myeve);	
	};
	*/
	

	ontouch(d('settings_table'), function(evt, dir, phase, swipetype, distance){
	 //if (dir === 'up' || dir === 'down')
	 //	return true;
	 if ((phase == 'end')&&((dir =='right')||(dir =='left'))){
		Array.prototype.forEach.call(qa(".settings_tree li a"), function(a,i) {
			if (hasClass(qa(".settings_tree li").item(i),'selected')){
				ielem = i%5;
			}
		});
		
		if (dir =='left'){
			if (ielem < 4){
				ielem += 1;
			}
			//ielem = (ielem + 1)%5;			
		}
		else{
			if (ielem > 0){
				ielem -= 1;
			}		
			//ielem = ((ielem - 1)+5)%5;
		}

		//qa(".settings_tree li a").item(ielem).click();
		var myeve = document.createEvent("HTMLEvents");
		myeve.initEvent("click", true, true);					
		qa(".settings_tree li a").item(ielem).dispatchEvent(myeve);		
	  };
	  //return false;
	});
	
	Array.prototype.forEach.call(qa("#menu li a[href^='#']"), function(el) {
		//el.onclick = function(e,a) {
		el.onclick = function(e) {
	
		//el.addEventListener('click', function(e, this.elements[i]){ )
	
		//var e = e || window.event; // IE compatibility
		//(e.preventDefault) ? e.preventDefault() : e.returnValue = false;
		
		var a = arg;
		
		var nli = this;
		
		if (back != true){
			node = this.hash.split(/[_?]/)[0];
			if (this.hash.split('_').length > 1){
				tab = this.hash.split('_')[1].split('?')[0];
			}
			else{
				tab = null;
			}
			/*
			//if (node == '#settings'){
			//	arg = 'e=sis';
			//}
			//if (arg == null){
			if (this.hash.split('?').length > 1){
				arg = this.hash.split('?')[1];
			}
			//}
			*/
			if (arg == null){
				if (this.hash == '#status'){
					arg = 'e=sis';
				}
			}
			//else if (this.hash.split('?').length > 1){
			//	arg = this.hash.split('?')[1];
			//}
			//else{
			//	arg = null;
			//}
		}
		
		Array.prototype.forEach.call(qa('#menu .selected'), function(e) {removeClass(e,'selected');});

		//this.parentNode.className += ' selected';
		addClass(this.parentNode,'selected');

		selectedId = this.hash.split(/[_?]/)[0] + '_id';
		Array.prototype.forEach.call(qa(".container"), function(e) {addClass(e,'hide');});
		removeClass(q(selectedId), 'hide');
		
		qa('.breadcrumb span')[1].textContent = " › " + this.childNodes[1].textContent;
		qa('.breadcrumb span')[2].textContent = '';
		qa('.breadcrumb span')[3].textContent = '';
		
		removeClass(q('#menuLink'), 'active');
        removeClass(q('#layout'), 'active');

		//var split = nli.hash.split('_');
/*
		var split = window.location.hash.split('_');
		if (split.length > 1){
			//var nav_sec = nli.hash + '_tab';
			var nav_sec = window.location.hash.split('?')[0] + '_tab';
			
			//q(selectedId + ' .navs a[href="'+nav_sec+'"]').click();
			var myeve = document.createEvent("HTMLEvents");
			myeve.initEvent("click", true, true);
			q(selectedId + ' .navs a[href="'+nav_sec+'"]').dispatchEvent(myeve);
		}
		else{
			originalHash = this.hash;
			//location.hash = this.hash;
		}
*/
		if (tab != null){
			var nav_sec = selectedId.split('_id')[0] + '_' + tab + '_tab';
			var myeve = document.createEvent("HTMLEvents");
			myeve.initEvent("click", true, true);
			q(selectedId + ' .navs a[href="'+nav_sec+'"]').dispatchEvent(myeve);
		}
		/*
		if (split[0] != originalHash.split('?')[0]){
			arg = split[1];

			//q('#menu li a[href="'+split[0]+'"]').click();

			//var myeve = document.createEvent("HTMLEvents");
			//myeve.initEvent("click", true, true);
			//q('#menu li a[href="'+split[0]+'"]').dispatchEvent(myeve);
			if (qa('#menu li a[href="'+newHash+'"]').length){
				var myeve = document.createEvent("HTMLEvents");
				myeve.initEvent("click", true, true);
				q('#menu li a[href="'+newHash+'"]').dispatchEvent(myeve);
			}
			else{
				var cid = split[0].split('_')[0] + '_id';
				//var nav_sec = selected + '_tab';
				//qa('#settings_id .navs a[href]')[0].hash;
				if (qa(cid + ' .navs a[href]').length){
					var nav_sec = qa(cid + ' .navs a[href]')[0].hash.split('_tab')[0];
				}
				else{
					var nav_sec = qa(cid + ' .tree a[href]')[0].hash.split('_li')[0];
				}
				
				if (qa('#menu li a[href="'+nav_sec+'"]').length){
					var myeve = document.createEvent("HTMLEvents");
					myeve.initEvent("click", true, true);
					q('#menu li a[href="'+nav_sec+'"]').dispatchEvent(myeve);
				}
			}
			
		}
		else{
			if (split.length > 1){
				if (split[1] != originalHash.split('?')[1]){
					//q('li a[href="'+newHash+'_li"]').click();
					var myeve = document.createEvent("HTMLEvents");
					myeve.initEvent("click", true, true);
					q('li a[href="'+newHash+'_li"]').dispatchEvent(myeve);
				}
			}
		}
		*/
		
		events_ajax.restart();
			
		if (selectedId == '#dashboard_id'){
			dashboard_ajax.restart();
			location.hash = node;
		}
		else {
			dashboard_ajax.cancel();
		}
			
		if (selectedId == '#status_id'){
			/*
			if(a == undefined) {
				a = 'e=sis';
			}
			arg = a;
			*/
			
			if(arg == undefined) {
				arg = 'e=sis';
			}
			//var li = this.hash + '_li';
			var li = '#status?' + arg + '_li';
			//q('.status_tree li a[href="'+li+'"]').click();
			var myeve = document.createEvent("HTMLEvents");
			myeve.initEvent("click", true, true);
			q('.status_tree li a[href="'+li+'"]').dispatchEvent(myeve);
		}
		else{
			status_ajax.cancel();
		}
		
		if (selectedId == '#settings_id'){
			//settings_ajax.restart();
			//$('.settings_tree li a:first').trigger('click');
		}
		else{
			settings_ajax.cancel();
			settings_his_ajax.cancel();
		}
		
		if (selectedId == '#control_id'){
			ielem = ielem_ant = 5;
			index = index_ant = 0;
			control_ajax.restart();
			location.hash = node;
		}
		else{
			control_ajax.cancel();
		}

		if (selectedId == '#alarms_id'){
			//ielem = ielem_ant = 6;
			//index = index_ant = 0;
			//alarms_ajax.restart();
			//$('.settings_tree li a:first').trigger('click');
			
			//$('[role=alarms_role] .nav-secondary a:first').trigger('click');
		}
		else{
			alarms_ajax.cancel();
		}
		
		if (selectedId == '#network_id'){
			ielem = ielem_ant = 5;
			index = index_ant = 0;
			network_ajax.restart();
		}
		else{
			network_ajax.cancel();
		}
		
		if (selectedId == '#equipo_id'){
			ielem = ielem_ant = 5;
			index = index_ant = 0;
			equipo_ajax.restart();
		}
		else{
			equipo_ajax.cancel();
		}
		
		if (selectedId == '#boot_id'){
			q("#pretty-input").value = '';

			paso('#boot',1);
			addClass(q('#upload'),'pure-button-disabled');
			addClass(q('#update'),'pure-button-disabled');

			q('#file_name_label').textContent = '';
			q('#file_size_label').textContent = '0 Bytes';			
			q('.md5l').value = '';
			q('.md5r').textContent = '--';
			removeClass(q('.md5r').parentNode,'g');
			addClass(q('.md5r').parentNode, 'r');

			q('#bar_upload').style.width = '0%';
			q('#percent_upload').textContent = '0%';
			q('#status_upload').innerHTML = '';
			q('#speed_upload').textContent = '';
			
			events_ajax.cancel();
			boot_ajax.restart();
			location.hash = node;
		}
		else{
			boot_ajax.cancel();
		}
		
		/*
		//var height_diff = document.documentElement.clientHeight - q(selectedId).clientHeight;
		var height_diff = document.documentElement.clientHeight - q('#main').clientHeight;
		if ( height_diff > 142 ) {
			height_diff -= 142;
			q('.footer').style['marginTop'] = height_diff  + 'px';
		}
		*/
		
		//if (originalHash.split(/[_?]/).length > 1){
		//	return false;
		//}
		
		//if (arg != null){
		//	return false;
		//}	

		originalHash = location.hash;
		back = false;
		
		return false;		
		
		};
	});
	
	BrowserDetect.init();
	
	if ((!checkfont('Segoe UI Symbol'))||(BrowserDetect.browser == 'Safari')){
		var s = document.styleSheets[1];
		if (s.insertRule) {
			s.insertRule('.uni{ display:none; }', 0);
			s.insertRule('.uni1{ display:none; }', 0);
			s.insertRule('.uni2{ display:none; }', 0);
			s.insertRule('.uni3{ display:none; }', 0);
			
			//s.insertRule('.nouni{ display:inline; }', 0);
			s.insertRule('.nouni{ display:inline; }',s.cssRules.length);
			
		} else {  // Internet Explorer before version 9
			s.addRule('.uni', 'display:none;', 0);
			s.addRule('.uni1', 'display:none;', 0);
			s.addRule('.uni2', 'display:none;', 0);
			s.addRule('.uni3', 'display:none;', 0);

			//s.addRule('.nouni', 'display:inline;', 0);			
			s.addRule('.nouni', 'display:inline;', s.rules.length);
		}
	}
	
	/*
	if	((BrowserDetect.browser == 'Explorer')&&(BrowserDetect.version < 9)){
	//if (isIE() < 9){
	//if (isIE()){
		var selected = '#status?e=sis';
		var sp = selected.split('?');
		//var sp = selected.split(/[_?]/);
		
		q('a[href="#dashboard"]').style.display = 'none';
	}
	else{
		var selected = window.location.hash ? window.location.hash : '#dashboard';
		var sp = selected.split('?');
		//var sp = selected.split(/[_?]/);
	}
	*/
	
	var selected = window.location.hash ? window.location.hash : '#dashboard';
	
	/*
	var sp = selected.split('?');
	arg = sp[1];
	//arg = sp[sp.length-1];
	
	//$('.nav-collapse li a[href="'+sp[0]+'"]').trigger('click',sp[1]);
	
	//q('.nav-collapse li a[href="'+sp[0]+'"]').click();
	
	var myeve = document.createEvent("HTMLEvents");
	myeve.initEvent("click", true, true);
	q('#menu li a[href="'+sp[0]+'"]').dispatchEvent(myeve);
	//q('#menu li a[href="'+sp[0]+'"]').click();
	*/

	/*
	//Node
	node = selected.split(/[_?]/)[0];
	
	//Tiene tab
	if (selected.split('_').length > 1){
		var cid = selected.split(/[_?]/)[0] + '_id';
		var nav_sec = qa(cid + ' .navs a[href]')[0].hash.split('_tab')[0];
		tab = selected.split('_')[1].split('?')[0];	//cfg
	}
	else{
		var nav_sec = selected.split(/[_?]/)[0];
		tab = null;
	}
	
	//Tiene arg
	if (selected.split('?').length > 1){
		arg = selected.split('?')[1];
	}
	else{
		arg = null;
	}
	
	if (qa('#menu li a[href="'+nav_sec+'"]').length){
		var myeve = document.createEvent("HTMLEvents");
		myeve.initEvent("click", true, true);
		back = true;
		q('#menu li a[href="'+nav_sec+'"]').dispatchEvent(myeve);
	}
	*/
	
	hashChanged();
	
//},false);
});

//});
