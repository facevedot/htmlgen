from setuptools import setup

setup(
    name='HTMLGen',
    version='0.1',
    packages=[''],
    url='',
    license='',
    author='facevedo',
    author_email='facevedo@zigor.com',
    description='Credits to Josh from Stackoverflow for bulk of code',
    install_requires=[
        'csscompressor',
        'bs4',
        'htmlmin',
        'requests',
    ]
)
